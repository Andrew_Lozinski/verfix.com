-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 27 2019 г., 23:11
-- Версия сервера: 5.5.53
-- Версия PHP: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `verfix`
--

-- --------------------------------------------------------

--
-- Структура таблицы `applications`
--

CREATE TABLE `applications` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `application_id` int(11) NOT NULL,
  `master_id` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `report` varchar(255) NOT NULL,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `applications`
--

INSERT INTO `applications` (`id`, `type`, `application_id`, `master_id`, `is_active`, `report`, `end_date`) VALUES
(7, 'Сервисная', 18, 11, -1, 'Отказано', '2019-05-26 10:11:31'),
(8, 'Выездная', 3, 12, 1, 'Отказано', '2019-05-25 17:11:48'),
(9, 'Запрос', 5, 11, 0, 'Ответ', '2019-05-27 16:37:14'),
(10, 'Сервисная', 19, 0, 1, 'Отказано', '2019-05-25 17:06:24');

-- --------------------------------------------------------

--
-- Структура таблицы `calculation`
--

CREATE TABLE `calculation` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_mail_answer` int(11) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `producer` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `serial` varchar(255) NOT NULL,
  `about` varchar(500) NOT NULL,
  `user_id` int(11) NOT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `calculation`
--

INSERT INTO `calculation` (`id`, `name`, `email`, `is_mail_answer`, `phone`, `producer`, `model`, `serial`, `about`, `user_id`, `price`, `date`, `status`) VALUES
(5, 'Лозинский Андрей Леонидович', 'andrew.lozinski.official@gmail.com', 1, '+380939860568', 'Dell', 'A76-G4', 'SD120391023', 'Хочу поменять матрицу монитора', 11, 455, '2019-05-22 15:55:15', 'Информация выдана заказчику');

-- --------------------------------------------------------

--
-- Структура таблицы `directions`
--

CREATE TABLE `directions` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` varchar(500) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `directions`
--

INSERT INTO `directions` (`id`, `title`, `text`, `date`) VALUES
(1, 'График работы', 'Пн-Пт, 9:00 - 19:30; Сб 10:00 - 16:00', '2019-05-24 06:06:59'),
(2, 'Некорректные заявки', 'Все заявки с некорректной информацией должны отклоняться.', '2019-05-24 06:12:34');

-- --------------------------------------------------------

--
-- Структура таблицы `faq_explanation`
--

CREATE TABLE `faq_explanation` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `answer` varchar(500) NOT NULL,
  `faq_name_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `faq_explanation`
--

INSERT INTO `faq_explanation` (`id`, `title`, `answer`, `faq_name_id`) VALUES
(1, 'Зачем проводить диагностику?', 'Проведение диагностики необходимо для того, чтобы определить, в чем конкретно заключается неисправность и какие детали нужны для ремонта. К сожалению, информации по серийному номеру и модели Вашего устройства недостаточно.', 1),
(2, 'Что такое диагностика, что в нее входит?', 'Диагностика - это полная проверка всех модулей и функций устройства, проводится для определения причины неисправности, подбора запасных частей и расходных материалов. В результате диагностики мы получаем перечень необходимых для замены комплектующих и можем определить для Вас точную стоимость ремонта. Оплачивать диагностику нужно только в случае отказа от проведения ремонта.', 1),
(3, 'Могу ли я от отказаться от проведения диагностики?', 'Безусловно, Вы можете отказаться от проведения диагностики. Но провести ремонт в этом случае мы сможем, только если вместе с устройством клиентом будет предоставлен список партномеров, необходимых для ремонта комплектующих, либо сами комплектующие. На ремонт, проведенный нами без диагностики всех узлов, мы, к сожалению, не сможем дать гарантию.', 1),
(4, 'Диагностику будете проводить в моем присутствии?', 'К сожалению, присутствовать при диагностике и ремонте невозможно. Все работы проводятся на специально оборудованном ремонтном участке, который находится на закрытой охраняемой территории.', 1),
(5, 'Сколько стоит ремонт?', 'Точную стоимость ремонта можно узнать после проведения диагностики. Также Вы можете заполнить форму “Онлайн запрос стоимости ремонта”. Некоторые виды ремонта имеют фиксированную цену, с ними можно ознакомится в разделе «Цены».', 2),
(6, 'Сколько будет длиться ремонт?', 'Продолжительность ремонта определяется сложностью проводимых работ, а также сроками поставки запасных частей. Как правило ремонт выполняется от 3 до 14 дней, а иногда – от 3 до 45 дней.', 2),
(7, 'Могу ли я ускорить процесс ремонта?', 'Гарантийные политики нашей компании обязывают нас закрывать ремонты в срок 3 рабочих дня при условии наличия всех необходимых комплектующих. Что касается платных ремонтов, Вы можете воспользоваться услугой Срочный ремонт, в рамках которой мы проводим ускоренную диагностику и ремонт в течение 2-3 рабочих дней при любых обстоятельствах.', 2),
(8, 'На каком этапе ремонта сейчас мое устройство?', 'Вы всегда можете контролировать процесс ремонта. Для этого существует несколько способов: В разделе Заявки о состоянии ремонта требуется ввести всего лишь серийный номер и номер сервисной квитанции. Или войдите в личный кабинет чтобы посмотреть этапы выполнения работ.', 2),
(9, 'Как я узнаю, что ремонт завершен?', 'По завершению ремонта Вы получите sms-оповещение на контактный номер телефона, указанный при оформлении устройства в ремонт, и сообщение на электронную почту.', 2),
(10, 'Может ли мое устройство из ремонта забрать другой человек?', 'Да, может. Для этого просим Вас заблаговременно уведомить об этом сотрудника центра обслуживания, в группу поддержки клиентов по телефону или в онлайн чат. Для получения устройства обязательно нужно будет иметь при себе оригинал квитанции ремонта и документ, удостоверяющий личность получателя.', 2),
(11, 'Я потерял квитанцию ремонта, смогу я забрать свое устройство?', 'При утере квитанции выдача устройства возможна только лицу, которое сдавало технику, при наличии документа, удостоверяющего личность. Также просим Вас в этом случае связаться с нами и сообщить об утере во избежание попыток получения устройства злоумышленниками.', 2),
(12, 'Моя информация на устройстве сохранится после ремонта?', 'К сожалению, ни сервисный центр, ни производитель Вашего устройства не несут ответственности за сохранность информации на устройстве. Поэтому настоятельно рекомендуем создать резервные копии данных перед обращением. Не смотря не вышесказанное, компания Verfix заботится о своих клиентах и всегда уточняет наличие важных данных на принимаемых в ремонт устройствах, стараясь при возможности, по предварительному согласованию, сохранить всю информацию.', 2),
(13, 'По какому адресу можно сдать устройство в ремонт и какой у вас график работы?', 'Адреса и графики работы центров обслуживания Verfix представлены в разделе Контакты.', 3),
(14, 'Можно у вас купить запчасть?', 'К сожалению, наш сервисный центр не занимается продажей запасных частей.', 3),
(15, 'Производите ли обновление ПО?', 'Да, мы можем предложить обновление версии BIOS, установку оригинальной операционной системы с последующей установкой необходимых драйверов. Эти услуги платные, в независимости от гарантийности устройства.', 3),
(16, 'Чистка ноутбука. Как часто надо делать?', 'Большинство производителей рекомендует проводить чистку системы охлаждения с интервалом от 6 мес до 1 года, в зависимости от условий эксплуатации ноутбука. Если есть изменения в работе устройства (быстрый нагрев, медлительность работы, шум куллера), самое время обратиться за профилактикой системы охлаждения.', 3),
(17, 'Нужно записываться, перед приходом в центр обслуживания?', 'Вы можете обратиться в любой центр обслуживания и без предварительной регистрации. Но если Вы хотите сэкономить время и ускорить процедуру заполнения документов при приеме, рекомендуем заполнить специальную форму.', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `faq_name`
--

CREATE TABLE `faq_name` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `faq_name`
--

INSERT INTO `faq_name` (`id`, `name`, `sort_order`, `status`) VALUES
(1, 'Часто задаваемые вопросы по диагностике', 0, 1),
(2, 'Общие вопросы по ремонту', 0, 1),
(3, 'Общие вопросы и ответы', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `field_repair`
--

CREATE TABLE `field_repair` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `home` varchar(255) NOT NULL,
  `office` varchar(255) NOT NULL,
  `producer` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `serial` varchar(255) NOT NULL,
  `about` varchar(500) NOT NULL,
  `is_field` int(11) NOT NULL DEFAULT '1',
  `is_urgently` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `field_repair`
--

INSERT INTO `field_repair` (`id`, `name`, `email`, `phone`, `street`, `home`, `office`, `producer`, `model`, `serial`, `about`, `is_field`, `is_urgently`, `user_id`, `price`, `date`, `status`) VALUES
(3, 'Лозинский Андрей Леонидович', 'andrew.lozinski.official@gmail.com', '+380939860568', 'Киевская', '15', '', 'Asus', 'K5398', 'AK92-243', 'Не включается', 1, 0, 11, 23, '2019-05-22 14:43:12', 'Выезд на место');

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `title`, `text`, `date`) VALUES
(1, 'Конкурс от Verfix Center «День влюбленных в гаджеты»', 'Verfix Center проводит праздничной конкурс по случаю дня всех влюбленных среди пользователей мессенджера Telegram!', '2019-02-07 08:30:00'),
(2, '8 марта – выходной день', 'Уважаемые клиенты, сообщаем вам, что 8 марта – выходной день в Verfix Center.', '2019-03-06 12:15:00'),
(3, 'График работы в праздничные дни', 'Команда Verfix Center от всей души поздравляет Вас с наступающим светлым праздником Пасхи, а также с Днем победы над нацизмом во Второй мировой войне и днем памяти и примирения.', '2019-04-23 05:26:00');

-- --------------------------------------------------------

--
-- Структура таблицы `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `post`
--

INSERT INTO `post` (`id`, `name`) VALUES
(1, 'Новая Почта'),
(2, 'Укрпочта');

-- --------------------------------------------------------

--
-- Структура таблицы `prices_addition`
--

CREATE TABLE `prices_addition` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `prices_addition`
--

INSERT INTO `prices_addition` (`id`, `name`, `price`) VALUES
(1, 'Срочный ремонт', 200),
(2, 'Выездной ремонт', 500),
(3, 'Доставка', 150);

-- --------------------------------------------------------

--
-- Структура таблицы `prices_explanation`
--

CREATE TABLE `prices_explanation` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `prices_name_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `prices_explanation`
--

INSERT INTO `prices_explanation` (`id`, `title`, `price`, `prices_name_id`) VALUES
(1, 'Замена процессора', 150, 1),
(2, 'Замена радиатора системы охлаждени', 50, 1),
(3, 'Замена термопасты на процессоре, на видеокарте', 100, 1),
(4, 'Замена видеокарты', 50, 1),
(5, 'Замена привода', 50, 1),
(6, 'Замена ОЗУ', 40, 1),
(7, 'Замена системной платы (материнской платы)', 200, 1),
(8, 'Замена блока питания (с укладкой)', 150, 1),
(9, 'Замена процессора', 240, 2),
(10, 'Замена разъема аудио', 70, 2),
(11, 'Замена разъема USB', 70, 2),
(12, 'Замена разъема питания', 100, 2),
(13, 'Замена системной платы (материнской платы)', 320, 2),
(14, 'Замена ОЗУ', 50, 2),
(15, 'Замена видеокарты', 240, 2),
(16, 'Замена клавиатуры', 140, 2),
(17, 'Замена матрицы', 300, 2),
(18, 'Замена жесткого диска, SSD диска, привода', 110, 2),
(19, 'Замена батареи', 30, 2),
(20, 'Замена элемента питания BIOS, UEF', 100, 2),
(21, 'Чистка системы охлаждения от пыли', 150, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `prices_name`
--

CREATE TABLE `prices_name` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `prices_name`
--

INSERT INTO `prices_name` (`id`, `name`, `sort_order`, `status`) VALUES
(1, 'Ремонт системных блоков', 0, 1),
(2, 'Ремонт ноутбуков', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `repair`
--

CREATE TABLE `repair` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `producer` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `serial` varchar(255) NOT NULL,
  `is_urgently` int(11) NOT NULL,
  `is_delivery` int(11) NOT NULL,
  `post_service` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `home` varchar(255) NOT NULL,
  `office` varchar(255) NOT NULL,
  `about` varchar(500) NOT NULL,
  `additionally` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `repair`
--

INSERT INTO `repair` (`id`, `name`, `email`, `phone`, `producer`, `model`, `serial`, `is_urgently`, `is_delivery`, `post_service`, `street`, `home`, `office`, `about`, `additionally`, `user_id`, `price`, `date`, `status`) VALUES
(18, 'Лозинский Андрей Леонидович', 'andrew.lozinski.official@gmail.com', '+380939860568', 'Iphone', '5-G', '002131HG4', 1, 1, 'Новая Почта', 'Киевская', '5', '', 'Разбит экран', 'Пароль экрана: 2567', 11, 200, '2019-05-22 14:50:19', 'Просчет стоимости'),
(19, 'Лозинский Андрей Леонидович', 'andrew.lozinski.official@gmail.com', '+380939860568', 'Samsung', 'Galaxy S3', 'A34838DSF432', 1, 0, '', '', '', '', 'Глючит система', '', 11, 0, '2019-05-24 12:23:31', 'Просчет стоимости');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL DEFAULT 'Пользователь'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `role`) VALUES
(11, 'Лозинский Андрей Леонидович', 'andrew.lozinski.official@gmail.com', 'sft561da38', '+380939860568', 'Администратор'),
(12, 'Гусев Филипп Семенович', 'gusev_semenovich@gmail.com', 'a2727uv7', '+380964785364', 'Мастер'),
(13, 'Красинец Платон Борисович', 'red.boy@gmail.com', '1234567', '+3801475685234', 'Пользователь');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `calculation`
--
ALTER TABLE `calculation`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `directions`
--
ALTER TABLE `directions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `faq_explanation`
--
ALTER TABLE `faq_explanation`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `faq_name`
--
ALTER TABLE `faq_name`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `field_repair`
--
ALTER TABLE `field_repair`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `prices_addition`
--
ALTER TABLE `prices_addition`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `prices_explanation`
--
ALTER TABLE `prices_explanation`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `prices_name`
--
ALTER TABLE `prices_name`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `repair`
--
ALTER TABLE `repair`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `applications`
--
ALTER TABLE `applications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `calculation`
--
ALTER TABLE `calculation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `directions`
--
ALTER TABLE `directions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `faq_explanation`
--
ALTER TABLE `faq_explanation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблицы `faq_name`
--
ALTER TABLE `faq_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `field_repair`
--
ALTER TABLE `field_repair`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `prices_addition`
--
ALTER TABLE `prices_addition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `prices_explanation`
--
ALTER TABLE `prices_explanation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблицы `prices_name`
--
ALTER TABLE `prices_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `repair`
--
ALTER TABLE `repair`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
