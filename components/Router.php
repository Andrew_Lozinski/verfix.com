<?php 

class Router
{
	private $routes; // массив в котором храниться маршруты

	// Прочитаем и запомним роуты
	public function __construct()
	{
		// Указывам путь к роутам
		$routesPath = ROOT.'/config/routes.php';
		// Присваиваем свойству routes масив который лежит в файле routes.php
		$this->routes = include($routesPath);
	}

	// Возвращает запрос в форме строки
	private function getURI()
	{
		if(!empty($_SERVER['REQUEST_URI'])) {
			return trim($_SERVER['REQUEST_URI'], '/');
		}
	}

	public function run() // принимает управление от FRONT CONTROLLER
	{
		// print_r($this->routes); // посмотрим роуты

		// Получить строку запроса //
		$uri = $this->getURI();
		// echo $uri;

		// Проверить наличие такого запроса в routes.php //
		foreach ($this->routes as $uriPattern => $path) {
			if(preg_match("~$uriPattern~", $uri)){ // тильда нужна для разделителя, ибо / может быть в запросе

				// Получаем внутренний путь из внешнего согласно правилу //

				$internalRoute = preg_replace("~$uriPattern~", $path, $uri);

				// Если есть совпадение, определить какой контроллер и action обрабатывает запрос //

				$segments = explode('/', $internalRoute);

				$controllerName = array_shift($segments).'Controller';
				$controllerName = ucfirst($controllerName);

				$actionName = 'action'.ucfirst(array_shift($segments));

				$parameters = $segments;
				// print_r($parameters);

				// Подключить файл класса-контроллера //
				$controllerFile = ROOT . '/controllers/' . $controllerName . '.php';

				if(file_exists($controllerFile))
					include_once($controllerFile);

				// Создать объект, вызвать метод (т.е. action) //
				$controllerObject = new $controllerName;
				
				// Ф-я вызывает action с именем $actionName у объкта $controllerObject, при этом передает ему массив с $parameters
				$result = call_user_func_array(array($controllerObject, $actionName), $parameters);

				if($result != null)
					break;
			} 
		}
	}
}