<?php

class MasterBase
{
	public static function checkMaster()
    {
        // Проверяем авторизирован ли пользователь. Если нет, он будет переадресован
        $userId = User::checkLogged();

        // Получаем информацию о текущем пользователе
        $user = User::getUserById($userId);

        // Если роль текущего пользователя "Мастер" или "Администратор", пускаем его в мастер панель
        if ($user['role'] == 'Мастер' || $user['role'] == 'Администратор') {
            return true;
        }

        // Иначе завершаем работу с сообщением об закрытом доступе
        die('<h1 style = "color: red;">Access denied</h1>');
    }
}