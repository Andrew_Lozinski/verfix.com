<?php

class AdminBase
{
    public static function checkAdmin()
    {
        // Проверяем авторизирован ли пользователь. Если нет, он будет переадресован
        $userId = User::checkLogged();

        // Получаем информацию о текущем пользователе
        $user = User::getUserById($userId);

        // Если роль текущего пользователя "Администратор", пускаем его в админ панель
        if ($user['role'] == 'Администратор') {
            return true;
        }

        // Иначе завершаем работу с сообщением об закрытом доступе
        die('<h1 style = "color: red;">Access denied</h1>');
    }
}