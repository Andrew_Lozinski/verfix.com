<?php

class Post
{
	public static function addPost($name)
	{
		// Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = 'INSERT INTO post (name) VALUES (:name)';
        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        
        return $result->execute();
	}

    public static function updatePost($id, $name)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "UPDATE post SET name = :name WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        
        return $result->execute();
    }

	public static function deletePost($id)
	{
		// Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "DELETE FROM post WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        return $result->execute();
	}

	public static function getPostList()
	{
		$db = Db::getConnection();

		$postList = array();

		$result = $db->query('SELECT * FROM post');

		$i = 0;
		while ($row = $result->fetch()) {
		 	$postList[$i]['id'] = $row['id'];
		 	$postList[$i]['name'] = $row['name'];
		 	$i++;
		 }

		 return $postList;
	}

	public static function getPostItemById($id)
	{
		$db = Db::getConnection();

		$result = $db->query('SELECT * FROM post WHERE id = ' . $id);
		$result->setFetchMode(PDO::FETCH_ASSOC);

		$categoryItem = $result->fetch();

		return $categoryItem;
	}
}