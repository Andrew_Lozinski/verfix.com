<?php

class Directions
{
	public static function addDirections($title, $text)
	{
		// Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = 'INSERT INTO directions (title, text)'
                . 'VALUES (:title, :text)';
        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':title', $title, PDO::PARAM_STR);
        $result->bindParam(':text', $text, PDO::PARAM_STR);

        
        return $result->execute();
	}

    public static function updateDirections($id, $title, $text)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "UPDATE directions SET title = :title, text = :text WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':title', $title, PDO::PARAM_STR);
        $result->bindParam(':text', $text, PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        
        return $result->execute();
    }

	public static function deleteDirections($id)
	{
		// Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "DELETE FROM directions WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        return $result->execute();
	}

	public static function getDirections()
	{
		$db = Db::getConnection();

		$directionsList = array();

		$result = $db->query('SELECT * FROM directions ORDER BY date DESC');

		$i = 0;
		while ($row = $result->fetch()) {
		 	$directionsList[$i]['id'] = $row['id'];
		 	$directionsList[$i]['title'] = $row['title'];
		 	$directionsList[$i]['text'] = $row['text'];
		 	$directionsList[$i]['date'] = $row['date'];
		 	$i++;
		 }

		 return $directionsList;
	}

	public static function getDirectionItemById($id)
	{
		$db = Db::getConnection();

		$result = $db->query('SELECT * FROM directions WHERE id = ' . $id);
		$result->setFetchMode(PDO::FETCH_ASSOC);

		$categoryItem = $result->fetch();

		return $categoryItem;
	}
}