<?php

class Faq
{

    public static function addCategory($name, $sort_order, $status)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "INSERT INTO faq_name (name, sort_order, status) VALUES (:name, :sort_order, :status)";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':sort_order', $sort_order, PDO::PARAM_INT);
        $result->bindParam(':status', $status, PDO::PARAM_INT);

        
        return $result->execute();
    }

    public static function updateCategory($id, $name, $sort_order, $status)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "UPDATE faq_name SET name = :name, sort_order = :sort_order, status = :status WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':sort_order', $sort_order, PDO::PARAM_INT);
        $result->bindParam(':status', $status, PDO::PARAM_INT);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        
        return $result->execute();
    }

	public static function deleteCategory($id)
	{
		// Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "DELETE FROM faq_name WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        $result->execute();

        // Текст запроса к БД
        $sql = "DELETE FROM faq_explanation WHERE faq_name_id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        return $result->execute();
	}

	public static function getCategoryItemById($id)
	{
		$db = Db::getConnection();

		$result = $db->query('SELECT * FROM faq_name WHERE id = ' . $id);
		$result->setFetchMode(PDO::FETCH_ASSOC);

		$categoryItem = $result->fetch();

		return $categoryItem;
	}

    public static function addFaq($title, $answer, $category)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "INSERT INTO faq_explanation (title, answer, faq_name_id) VALUES (:title, :answer, :category)";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':title', $title, PDO::PARAM_STR);
        $result->bindParam(':answer', $answer, PDO::PARAM_INT);
        $result->bindParam(':category', $category, PDO::PARAM_INT);

        
        return $result->execute();
    }

    public static function updateFaq($id, $title, $answer, $category)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "UPDATE faq_explanation SET title = :title, answer = :answer, faq_name_id = :category WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':title', $title, PDO::PARAM_STR);
        $result->bindParam(':answer', $answer, PDO::PARAM_STR);
        $result->bindParam(':category', $category, PDO::PARAM_INT);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        
        return $result->execute();
    }

	public static function deleteFaq($id)
	{
		// Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "DELETE FROM faq_explanation WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        return $result->execute();
	}

	public static function getFaqItemById($id)
	{
		$db = Db::getConnection();

		$result = $db->query('SELECT * FROM faq_explanation WHERE id = ' . $id);
		$result->setFetchMode(PDO::FETCH_ASSOC);

		$categoryItem = $result->fetch();

		return $categoryItem;
	}

	public static function getFaqList()
	{
		$db = Db::getConnection();

		$faqList = array();

		$result = $db->query('SELECT * FROM faq_name WHERE status=1 ORDER BY sort_order DESC');

		$i = 0;
		while ($row = $result->fetch()) {
		 	$faqList[$i]['id'] = $row['id'];
		 	$faqList[$i]['name'] = $row['name'];
		 	$i++;
		}

		 return $faqList;
	}

	public static function getFaqItemsList()
	{
		$db = Db::getConnection();

		$faqItemsList = array();

		$result = $db->query('SELECT * FROM faq_explanation');

		$i = 0;
		while ($row = $result->fetch()) {
		 	$faqItemsList[$i]['id'] = $row['id'];
		 	$faqItemsList[$i]['title'] = $row['title'];
		 	$faqItemsList[$i]['answer'] = $row['answer'];
		 	$faqItemsList[$i]['faq_name_id'] = $row['faq_name_id'];
		 	$i++;
		}

		 return $faqItemsList;
	}

}