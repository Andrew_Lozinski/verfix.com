<?php

class News
{
	public static function add($title, $text)
	{
		// Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = 'INSERT INTO news (title, text)'
                . 'VALUES (:title, :text)';
        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':title', $title, PDO::PARAM_STR);
        $result->bindParam(':text', $text, PDO::PARAM_STR);

        
        if ($result->execute()) {
            // Если запрос выполенен успешно, возвращаем id добавленной записи
            return $db->lastInsertId();
        }
        // Иначе возвращаем 0
        return 0;
	}

    public static function update($id, $title, $text)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "UPDATE news SET title = :title, text = :text WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':title', $title, PDO::PARAM_STR);
        $result->bindParam(':text', $text, PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        
        return $result->execute();
    }

    public static function delete($id)
	{
		// Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "DELETE FROM news WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        // Путь к папке с фотографиями
        $path = '/data/news/';

        // Путь к изображению
        $pathToNewsImage = $path . $id . '.jpg';

        if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToNewsImage)) {
            // Если изображение новости существует
            // Удаляем изображения
            unlink($_SERVER['DOCUMENT_ROOT'].$pathToNewsImage);
        }
       
        return $result->execute();
	}

	public static function getNewsItemById($id)
	{
		$id = intval($id);

		if($id) {
			$db = Db::getConnection();

			$result = $db->query('SELECT * FROM news WHERE id = ' . $id);
			$result->setFetchMode(PDO::FETCH_ASSOC);

			$newsItem = $result->fetch();

			return $newsItem;
		}
	}

	public static function getNewsList()
	{
		$db = Db::getConnection();

		$newsList = array();

		$result = $db->query('SELECT * FROM news ORDER BY date DESC');

		$i = 0;
		while ($row = $result->fetch()) {
		 	$newsList[$i]['id'] = $row['id'];
		 	$newsList[$i]['title'] = $row['title'];
		 	$newsList[$i]['text'] = $row['text'];
		 	$newsList[$i]['date'] = $row['date'];
		 	$i++;
		 }

		 return $newsList;
	}

	public static function getNewsPic($id)
    {
        // Название изображения-пустышки
        $noImage = 'news.jpg';

        // Путь к папке с фотографиями
        $path = '/data/news/';

        // Путь к изображению
        $pathToNewsImage = $path . $id . '.jpg';

        if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToNewsImage)) {
            // Если изображение новости существует
            // Возвращаем путь изображения
            return $pathToNewsImage;
        }

        // Возвращаем путь изображения-пустышки
        return '/data/news/' . $noImage;
    }
}