<?php

class Prices
{

    public static function addCategory($name, $sort_order, $status)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "INSERT INTO prices_name (name, sort_order, status) VALUES (:name, :sort_order, :status)";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':sort_order', $sort_order, PDO::PARAM_INT);
        $result->bindParam(':status', $status, PDO::PARAM_INT);

        
        return $result->execute();
    }

    public static function updateCategory($id, $name, $sort_order, $status)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "UPDATE prices_name SET name = :name, sort_order = :sort_order, status = :status WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':sort_order', $sort_order, PDO::PARAM_INT);
        $result->bindParam(':status', $status, PDO::PARAM_INT);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        
        return $result->execute();
    }

	public static function deleteCategory($id)
	{
		// Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "DELETE FROM prices_name WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        $result->execute();

        // Текст запроса к БД
        $sql = "DELETE FROM prices_explanation WHERE prices_name_id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        return $result->execute();
	}

	public static function getCategoryItemById($id)
	{
		$db = Db::getConnection();

		$result = $db->query('SELECT * FROM prices_name WHERE id = ' . $id);
		$result->setFetchMode(PDO::FETCH_ASSOC);

		$categoryItem = $result->fetch();

		return $categoryItem;
	}

	public static function getAdditionItemById($id)
	{
		$db = Db::getConnection();

		$result = $db->query('SELECT * FROM prices_addition WHERE id = ' . $id);
		$result->setFetchMode(PDO::FETCH_ASSOC);

		$additionItem = $result->fetch();

		return $additionItem;
	}

    public static function updateAddition($id, $price)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "UPDATE prices_addition SET price = :price WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':price', $price, PDO::PARAM_INT);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        
        return $result->execute();
    }

	public static function getPriceItemById($id)
	{
		$db = Db::getConnection();

		$result = $db->query('SELECT * FROM prices_explanation WHERE id = ' . $id);
		$result->setFetchMode(PDO::FETCH_ASSOC);

		$categoryItem = $result->fetch();

		return $categoryItem;
	}

    public static function addPrice($name, $price, $category)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "INSERT INTO prices_explanation (title, price, prices_name_id) VALUES (:name, :price, :category)";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':price', $price, PDO::PARAM_INT);
        $result->bindParam(':category', $category, PDO::PARAM_INT);

        
        return $result->execute();
    }

    public static function updatePrice($id, $name, $price, $category)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "UPDATE prices_explanation SET title = :name, price = :price, prices_name_id = :category WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':price', $price, PDO::PARAM_INT);
        $result->bindParam(':category', $category, PDO::PARAM_INT);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        
        return $result->execute();
    }

	public static function deletePrice($id)
	{
		// Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "DELETE FROM prices_explanation WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        return $result->execute();
	}


	public static function getPricesList()
	{
		$db = Db::getConnection();

		$pricesList = array();

		$result = $db->query('SELECT * FROM prices_name WHERE status=1 ORDER BY sort_order DESC');

		$i = 0;
		while ($row = $result->fetch()) {
		 	$pricesList[$i]['id'] = $row['id'];
		 	$pricesList[$i]['name'] = $row['name'];
		 	$i++;
		 }

		 return $pricesList;
	}

	public static function getPricesItemsList()
	{
		$db = Db::getConnection();

		$pricesItemsList = array();

		$result = $db->query('SELECT * FROM prices_explanation');

		$i = 0;
		while ($row = $result->fetch()) {
		 	$pricesItemsList[$i]['id'] = $row['id'];
		 	$pricesItemsList[$i]['title'] = $row['title'];
		 	$pricesItemsList[$i]['price'] = $row['price'];
		 	$pricesItemsList[$i]['prices_name_id'] = $row['prices_name_id'];
		 	$i++;
		 }

		 return $pricesItemsList;
	}

	public static function getAdditionList()
	{
		$db = Db::getConnection();

		$pricesAdditionList = array();

		$result = $db->query('SELECT * FROM prices_addition');

		$i = 0;
		while ($row = $result->fetch()) {
			$pricesAdditionList[$i]['id'] = $row['id'];
		 	$pricesAdditionList[$i]['name'] = $row['name'];
		 	$pricesAdditionList[$i]['price'] = $row['price'];
		 	$i++;
		 }

		 return $pricesAdditionList;
	}
}