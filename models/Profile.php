<?php

class Profile
{
	
    public static function getRepairApplicationsByUserId($id)
    {
        // Соединение с БД
        $db = Db::getConnection();

        $applicationsList = array();

        // Текст запроса к БД
        $result = $db->query('SELECT * FROM repair INNER JOIN applications ON applications.application_id = repair.id WHERE user_id = 11 ORDER BY date DESC');

        $i = 0;
		while ($row = $result->fetch()) {
		 	$applicationsList[$i]['id'] = $row['id'];
		 	$applicationsList[$i]['name'] = $row['name'];
		 	$applicationsList[$i]['producer'] = $row['producer'];
		 	$applicationsList[$i]['model'] = $row['model'];
		 	$applicationsList[$i]['serial'] = $row['serial'];
		 	$applicationsList[$i]['is_urgently'] = $row['is_urgently'];
		 	$applicationsList[$i]['is_delivery'] = $row['is_delivery'];
		 	$applicationsList[$i]['post_service'] = $row['post_service'];
		 	$applicationsList[$i]['about'] = $row['about'];
		 	$applicationsList[$i]['price'] = $row['price'];
		 	$applicationsList[$i]['date'] = $row['date'];
		 	$applicationsList[$i]['status'] = $row['status'];

            $applicationsList[$i]['is_active'] = $row['is_active'];
            $applicationsList[$i]['report'] = $row['report'];
		 	$i++;
		 }

        return  $applicationsList;
    }

    public static function getFieldApplicationsByUserId($id)
    {
        // Соединение с БД
        $db = Db::getConnection();

        $applicationsList = array();

        // Текст запроса к БД
        $result = $db->query('SELECT * FROM field_repair INNER JOIN applications ON applications.application_id = field_repair.id WHERE user_id = '.$id.' ORDER BY date DESC');

        $i = 0;
        while ($row = $result->fetch()) {
            $applicationsList[$i]['id'] = $row['id'];
            $applicationsList[$i]['name'] = $row['name'];
            $applicationsList[$i]['producer'] = $row['producer'];
            $applicationsList[$i]['model'] = $row['model'];
            $applicationsList[$i]['serial'] = $row['serial'];
            $applicationsList[$i]['is_field'] = $row['is_field'];
            $applicationsList[$i]['is_urgently'] = $row['is_urgently'];
            $applicationsList[$i]['about'] = $row['about'];
            $applicationsList[$i]['price'] = $row['price'];
            $applicationsList[$i]['date'] = $row['date'];
            $applicationsList[$i]['status'] = $row['status'];

            $applicationsList[$i]['is_active'] = $row['is_active'];
            $applicationsList[$i]['report'] = $row['report'];
            $i++;
         }

        return  $applicationsList;
    }

    public static function getCalculationApplicationsByUserId($id)
    {
        // Соединение с БД
        $db = Db::getConnection();

        $applicationsList = array();

        // Текст запроса к БД
        $result = $db->query('SELECT * FROM calculation INNER JOIN applications ON applications.application_id = calculation.id WHERE user_id = '.$id.' ORDER BY date DESC');

        $i = 0;
        while ($row = $result->fetch()) {
            $applicationsList[$i]['id'] = $row['id'];
            $applicationsList[$i]['name'] = $row['name'];
            $applicationsList[$i]['producer'] = $row['producer'];
            $applicationsList[$i]['model'] = $row['model'];
            $applicationsList[$i]['serial'] = $row['serial'];
            $applicationsList[$i]['about'] = $row['about'];
            $applicationsList[$i]['price'] = $row['price'];
            $applicationsList[$i]['date'] = $row['date'];
            $applicationsList[$i]['status'] = $row['status'];

            $applicationsList[$i]['is_active'] = $row['is_active'];
            $applicationsList[$i]['report'] = $row['report'];
            $i++;
         }

        return  $applicationsList;
    }

}