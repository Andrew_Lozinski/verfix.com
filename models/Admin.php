<?php

class Admin
{

	public static function getAllStatistics()
	{
		$db = Db::getConnection();

		$statisticsList = array();

		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE type = 'Сервисная' AND master_id != 0 AND is_active = 0");

		$row = $result->fetch();

		$statisticsList['repair_complete'] = $row['count'];

		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE type = 'Сервисная' AND master_id != 0 AND is_active = -1");

		$row = $result->fetch();

		$statisticsList['repair_cancel'] = $row['count'];



		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE type = 'Выездная' AND master_id != 0 AND is_active = 0");

		$row = $result->fetch();

		$statisticsList['field_repair_complete'] = $row['count'];

		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE type = 'Выездная' AND master_id != 0 AND is_active = -1");

		$row = $result->fetch();

		$statisticsList['field_repair_cancel'] = $row['count'];


		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE type = 'Запрос' AND master_id != 0 AND is_active = 0");

		$row = $result->fetch();

		$statisticsList['calculation_complete'] = $row['count'];

		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE type = 'Запрос' AND master_id != 0 AND is_active = -1");

		$row = $result->fetch();

		$statisticsList['calculation_cancel'] = $row['count'];


		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE master_id != 0 AND is_active = 0");

		$row = $result->fetch();

		$statisticsList['complete'] = $row['count'];

		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE master_id != 0 AND is_active = -1");

		$row = $result->fetch();

		$statisticsList['cancel'] = $row['count'];

		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE master_id = 0 AND is_active = 1");

		$row = $result->fetch();

		$statisticsList['queue'] = $row['count'];

		return $statisticsList;
	}

	public static function getUsersList()
	{
		$db = Db::getConnection();

		$usersList = array();

		$result = $db->query('SELECT * FROM users');

		$i = 0;
		while ($row = $result->fetch()) {
		 	$usersList[$i]['id'] = $row['id'];
		 	$usersList[$i]['name'] = $row['name'];
		 	$usersList[$i]['email'] = $row['email'];
		 	$usersList[$i]['role'] = $row['role'];
		 	$i++;
		 }

		 return $usersList;
	}

	public static function updateUser($id, $role)
	{
		// Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "UPDATE users SET role = :role WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':role', $role, PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        return $result->execute();
	}

	public static function getPricesList()
	{
		$db = Db::getConnection();

		$pricesList = array();

		$result = $db->query('SELECT * FROM prices_name');

		$i = 0;
		while ($row = $result->fetch()) {
		 	$pricesList[$i]['id'] = $row['id'];
		 	$pricesList[$i]['name'] = $row['name'];
		 	$pricesList[$i]['sort_order'] = $row['sort_order'];
		 	$pricesList[$i]['status'] = $row['status'];
		 	$i++;
		 }

		 return $pricesList;
	}


	public static function getPricesItemsList()
	{
		$db = Db::getConnection();

		$pricesItemsList = array();

		$result = $db->query('SELECT * FROM prices_name INNER JOIN prices_explanation ON prices_explanation.prices_name_id = prices_name.id');

		$i = 0;
		while ($row = $result->fetch()) {
		 	$pricesItemsList[$i]['id'] = $row['id'];
		 	$pricesItemsList[$i]['title'] = $row['title'];
		 	$pricesItemsList[$i]['price'] = $row['price'];
		 	$pricesItemsList[$i]['category'] = $row['name'];
		 	$i++;
		 }

		 return $pricesItemsList;
	}

	public static function getFaqList()
	{
		$db = Db::getConnection();

		$faqList = array();

		$result = $db->query('SELECT * FROM faq_name');

		$i = 0;
		while ($row = $result->fetch()) {
		 	$faqList[$i]['id'] = $row['id'];
		 	$faqList[$i]['name'] = $row['name'];
		 	$faqList[$i]['sort_order'] = $row['sort_order'];
		 	$faqList[$i]['status'] = $row['status'];
		 	$i++;
		}

		 return $faqList;
	}

	public static function getFaqItemsList()
	{
		$db = Db::getConnection();

		$pricesItemsList = array();

		$result = $db->query('SELECT * FROM faq_name INNER JOIN faq_explanation ON faq_explanation.faq_name_id = faq_name.id');

		$i = 0;
		while ($row = $result->fetch()) {
		 	$pricesItemsList[$i]['id'] = $row['id'];
		 	$pricesItemsList[$i]['title'] = $row['title'];
		 	$pricesItemsList[$i]['answer'] = $row['answer'];
		 	$pricesItemsList[$i]['category'] = $row['name'];
		 	$i++;
		 }

		 return $pricesItemsList;
	}

	public static function checkFill($parameter, $n)
    {
        if (mb_strlen($parameter, 'utf8') >= $n) {
            return true;
        }
        return false;
    }

	public static function removeMaster($applicationId)
	{
		// Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД Выездная
        $result = $db->query("UPDATE applications SET master_id=0, is_active = 1 WHERE id = $applicationId");

        return $result->execute();
	}

	public static function getInProgressApplicationsList()
	{
		// Соединение с БД
        $db = Db::getConnection();

        $applicationsList = array();

        // Текст запроса к БД Сервисная
        $result = $db->query('SELECT applications.id, about, producer, model, serial, repair.date, users.name AS master FROM applications INNER JOIN repair ON application_id = repair.id INNER JOIN users ON master_id = users.id WHERE applications.is_active = 1');

        $i = 0;
		while ($row = $result->fetch()) {
		 	$applicationsList[$i]['id'] = $row['id'];
		 	$applicationsList[$i]['about'] = $row['about'];
		 	$applicationsList[$i]['producer'] = $row['producer'];
		 	$applicationsList[$i]['model'] = $row['model'];
			$applicationsList[$i]['serial'] = $row['serial'];
		 	$applicationsList[$i]['date'] = $row['date'];
		 	$applicationsList[$i]['type'] = 'Сервисная';
		 	$applicationsList[$i]['master'] = $row['master'];
		 	$i++;
		 }

		// Текст запроса к БД Выездная
        $result = $db->query('SELECT applications.id, about, producer, model, serial, field_repair.date, users.name AS master FROM applications INNER JOIN field_repair ON application_id = field_repair.id INNER JOIN users ON master_id = users.id WHERE applications.is_active = 1');

		while ($row = $result->fetch()) {
		 	$applicationsList[$i]['id'] = $row['id'];
		 	$applicationsList[$i]['about'] = $row['about'];
		 	$applicationsList[$i]['producer'] = $row['producer'];
		 	$applicationsList[$i]['model'] = $row['model'];
			$applicationsList[$i]['serial'] = $row['serial'];
		 	$applicationsList[$i]['date'] = $row['date'];
		 	$applicationsList[$i]['type'] = 'Сервисная';
		 	$applicationsList[$i]['master'] = $row['master'];
		 	$i++;
		}

		// Текст запроса к БД Запрос
        $result = $db->query('SELECT applications.id, about, producer, model, serial, calculation.date, users.name AS master FROM applications INNER JOIN calculation ON application_id = calculation.id INNER JOIN users ON master_id = users.id WHERE applications.is_active = 1');

		while ($row = $result->fetch()) {
		 	$applicationsList[$i]['id'] = $row['id'];
		 	$applicationsList[$i]['about'] = $row['about'];
		 	$applicationsList[$i]['producer'] = $row['producer'];
		 	$applicationsList[$i]['model'] = $row['model'];
			$applicationsList[$i]['serial'] = $row['serial'];
		 	$applicationsList[$i]['date'] = $row['date'];
		 	$applicationsList[$i]['type'] = 'Сервисная';
		 	$applicationsList[$i]['master'] = $row['master'];
		 	$i++;
		 }

        return  $applicationsList;
	}

	public static function getCanceledApplicationsList()
	{
		// Соединение с БД
        $db = Db::getConnection();

        $applicationsList = array();

        // Текст запроса к БД Сервисная
        $result = $db->query('SELECT applications.id, about, producer, model, serial, repair.date, users.name AS master FROM applications INNER JOIN repair ON application_id = repair.id INNER JOIN users ON master_id = users.id WHERE applications.is_active = -1');

        $i = 0;
		while ($row = $result->fetch()) {
		 	$applicationsList[$i]['id'] = $row['id'];
		 	$applicationsList[$i]['about'] = $row['about'];
		 	$applicationsList[$i]['producer'] = $row['producer'];
		 	$applicationsList[$i]['model'] = $row['model'];
			$applicationsList[$i]['serial'] = $row['serial'];
		 	$applicationsList[$i]['date'] = $row['date'];
		 	$applicationsList[$i]['type'] = 'Сервисная';
		 	$applicationsList[$i]['master'] = $row['master'];
		 	$i++;
		 }

		// Текст запроса к БД Выездная
        $result = $db->query('SELECT applications.id, about, producer, model, serial, field_repair.date, users.name AS master FROM applications INNER JOIN field_repair ON application_id = field_repair.id INNER JOIN users ON master_id = users.id WHERE applications.is_active = -1');

		while ($row = $result->fetch()) {
		 	$applicationsList[$i]['id'] = $row['id'];
		 	$applicationsList[$i]['about'] = $row['about'];
		 	$applicationsList[$i]['producer'] = $row['producer'];
		 	$applicationsList[$i]['model'] = $row['model'];
			$applicationsList[$i]['serial'] = $row['serial'];
		 	$applicationsList[$i]['date'] = $row['date'];
		 	$applicationsList[$i]['type'] = 'Сервисная';
		 	$applicationsList[$i]['master'] = $row['master'];
		 	$i++;
		}

		// Текст запроса к БД Запрос
        $result = $db->query('SELECT applications.id, about, producer, model, serial, calculation.date, users.name AS master FROM applications INNER JOIN calculation ON application_id = calculation.id INNER JOIN users ON master_id = users.id WHERE applications.is_active = -1');

		while ($row = $result->fetch()) {
		 	$applicationsList[$i]['id'] = $row['id'];
		 	$applicationsList[$i]['about'] = $row['about'];
		 	$applicationsList[$i]['producer'] = $row['producer'];
		 	$applicationsList[$i]['model'] = $row['model'];
			$applicationsList[$i]['serial'] = $row['serial'];
		 	$applicationsList[$i]['date'] = $row['date'];
		 	$applicationsList[$i]['type'] = 'Сервисная';
		 	$applicationsList[$i]['master'] = $row['master'];
		 	$i++;
		 }

        return  $applicationsList;
	}

	public static function getCompletedApplicationsList()
	{
		// Соединение с БД
        $db = Db::getConnection();

        $applicationsList = array();

        // Текст запроса к БД Сервисная
        $result = $db->query('SELECT applications.id, about, producer, model, serial, repair.date, users.name AS master FROM applications INNER JOIN repair ON application_id = repair.id INNER JOIN users ON master_id = users.id WHERE applications.is_active = 0');

        $i = 0;
		while ($row = $result->fetch()) {
		 	$applicationsList[$i]['id'] = $row['id'];
		 	$applicationsList[$i]['about'] = $row['about'];
		 	$applicationsList[$i]['producer'] = $row['producer'];
		 	$applicationsList[$i]['model'] = $row['model'];
			$applicationsList[$i]['serial'] = $row['serial'];
		 	$applicationsList[$i]['date'] = $row['date'];
		 	$applicationsList[$i]['type'] = 'Сервисная';
		 	$applicationsList[$i]['master'] = $row['master'];
		 	$i++;
		 }

		// Текст запроса к БД Выездная
        $result = $db->query('SELECT applications.id, about, producer, model, serial, field_repair.date, users.name AS master FROM applications INNER JOIN field_repair ON application_id = field_repair.id INNER JOIN users ON master_id = users.id WHERE applications.is_active = 0');

		while ($row = $result->fetch()) {
		 	$applicationsList[$i]['id'] = $row['id'];
		 	$applicationsList[$i]['about'] = $row['about'];
		 	$applicationsList[$i]['producer'] = $row['producer'];
		 	$applicationsList[$i]['model'] = $row['model'];
			$applicationsList[$i]['serial'] = $row['serial'];
		 	$applicationsList[$i]['date'] = $row['date'];
		 	$applicationsList[$i]['type'] = 'Сервисная';
		 	$applicationsList[$i]['master'] = $row['master'];
		 	$i++;
		}

		// Текст запроса к БД Запрос
        $result = $db->query('SELECT applications.id, about, producer, model, serial, calculation.date, users.name AS master FROM applications INNER JOIN calculation ON application_id = calculation.id INNER JOIN users ON master_id = users.id WHERE applications.is_active = 0');

		while ($row = $result->fetch()) {
		 	$applicationsList[$i]['id'] = $row['id'];
		 	$applicationsList[$i]['about'] = $row['about'];
		 	$applicationsList[$i]['producer'] = $row['producer'];
		 	$applicationsList[$i]['model'] = $row['model'];
			$applicationsList[$i]['serial'] = $row['serial'];
		 	$applicationsList[$i]['date'] = $row['date'];
		 	$applicationsList[$i]['type'] = 'Сервисная';
		 	$applicationsList[$i]['master'] = $row['master'];
		 	$i++;
		 }

        return  $applicationsList;
	}
}