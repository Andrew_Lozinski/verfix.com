<?php

class Master
{
	public static function getAvailableApplicationsList()
	{
		// Соединение с БД
        $db = Db::getConnection();

        $applicationsList = array();

        // Текст запроса к БД Сервисная
        $result = $db->query('SELECT * FROM repair INNER JOIN applications ON applications.application_id = repair.id WHERE applications.master_id = 0 AND is_active=1 ORDER BY date ASC');

        $i = 0;
		while ($row = $result->fetch()) {
		 	$applicationsList[$i]['id'] = $row['id'];
		 	$applicationsList[$i]['about'] = $row['about'];
		 	$applicationsList[$i]['producer'] = $row['producer'];
		 	$applicationsList[$i]['model'] = $row['model'];
			$applicationsList[$i]['serial'] = $row['serial'];
		 	$applicationsList[$i]['date'] = $row['date'];
		 	$applicationsList[$i]['type'] = 'Сервисная';
		 	$applicationsList[$i]['is_urgently'] = $row['is_urgently'];
		 	$applicationsList[$i]['is_delivery'] = $row['is_delivery'];
		 	$applicationsList[$i]['is_field'] = 0;
		 	$applicationsList[$i]['is_mail_answer'] = 0;
		 	$i++;
		 }

		// Текст запроса к БД Выездная
        $result = $db->query('SELECT * FROM field_repair INNER JOIN applications ON applications.application_id = field_repair.id WHERE applications.master_id = 0 AND is_active=1 ORDER BY date ASC');

		while ($row = $result->fetch()) {
		 	$applicationsList[$i]['id'] = $row['id'];
		 	$applicationsList[$i]['about'] = $row['about'];
		 	$applicationsList[$i]['producer'] = $row['producer'];
		 	$applicationsList[$i]['model'] = $row['model'];
			$applicationsList[$i]['serial'] = $row['serial'];
		 	$applicationsList[$i]['date'] = $row['date'];
		 	$applicationsList[$i]['type'] = 'Выездная';
		 	$applicationsList[$i]['is_urgently'] = $row['is_urgently'];
		 	$applicationsList[$i]['is_field'] = $row['is_field'];
		 	$applicationsList[$i]['is_delivery'] = 0;
		 	$applicationsList[$i]['is_mail_answer'] = 0;
		 	$i++;
		}

		// Текст запроса к БД Запрос
        $result = $db->query('SELECT * FROM calculation INNER JOIN applications ON applications.application_id = calculation.id WHERE applications.master_id = 0 AND is_active=1 ORDER BY date ASC');

		while ($row = $result->fetch()) {
		 	$applicationsList[$i]['id'] = $row['id'];
		 	$applicationsList[$i]['about'] = $row['about'];
		 	$applicationsList[$i]['producer'] = $row['producer'];
		 	$applicationsList[$i]['model'] = $row['model'];
			$applicationsList[$i]['serial'] = $row['serial'];
		 	$applicationsList[$i]['date'] = $row['date'];
		 	$applicationsList[$i]['type'] = 'Запрос';
		 	$applicationsList[$i]['is_urgently'] = 0;
		 	$applicationsList[$i]['is_delivery'] = 0;
		 	$applicationsList[$i]['is_field'] = 0;
		 	$applicationsList[$i]['is_mail_answer'] = $row['is_mail_answer'];
		 	$i++;
		 }

        return  $applicationsList;
	}

	public static function getCountAvailable()
	{
		// Соединение с БД
        $db = Db::getConnection();

        $result = $db->query('SELECT COUNT(*) AS count FROM applications WHERE master_id = 0 AND is_active=1');

        $count = $result->fetch();

        return $count['count'];
	}

	public static function isMasterAvailable($userId)
	{
		// Соединение с БД
        $db = Db::getConnection();

        $result = $db->query('SELECT COUNT(*) AS count FROM applications WHERE master_id = '.$userId.' AND is_active=1');

        $count = $result->fetch();

        if($count['count'] == 0)
        	return true;
        else
        	return false;
	}

	public static function add($id, $userId)
	{
		// Соединение с БД
        $db = Db::getConnection();

        $result = $db->query("UPDATE applications SET master_id=$userId WHERE id = $id");

        return $result->execute();
	}

	public static function cancel($id, $userId)
	{
		// Соединение с БД
        $db = Db::getConnection();

        $result = $db->query("UPDATE applications SET master_id=$userId, is_active = -1, report='Отказано', end_date = NOW() WHERE id = $id");

        return $result->execute();
	}

	public static function getCurrentApplication($userId)
	{
		$db = Db::getConnection();

        $table = self::getCurrentApplicationType($userId);

		$result = $db->query("SELECT * FROM $table INNER JOIN applications ON $table.id = applications.application_id WHERE applications.master_id = $userId AND applications.is_active = 1");
		$result->setFetchMode(PDO::FETCH_ASSOC);

		$applicationItem = $result->fetch();

		return $applicationItem;
	}

	public static function getCurrentApplicationType($userId)
	{
		$db = Db::getConnection();

		$result = $db->query('SELECT type FROM applications WHERE master_id = '.$userId.' AND is_active = 1');

        $type = $result->fetch();

        if($type['type'] == 'Сервисная')
        	$table = 'repair';
        else if($type['type'] == 'Выездная')
        	$table = 'field_repair';
        else
        	$table = 'calculation';

		return $table;
	}

	public static function update($status, $price, $report, $applicationId, $table)
	{
		// Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "UPDATE $table INNER JOIN applications ON $table.id = applications.application_id SET $table.status = :status, $table.price = :price, applications.report = :report WHERE applications.id = $applicationId";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':status', $status, PDO::PARAM_STR);
        $result->bindParam(':price', $price, PDO::PARAM_INT);
        $result->bindParam(':report', $report, PDO::PARAM_STR);
        return $result->execute();
	}

	public static function ignore($userId, $applicationId)
	{
		// Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "UPDATE applications SET master_id = 0 WHERE applications.id = :applicationId AND master_id = :userId";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':userId', $userId, PDO::PARAM_INT);
        $result->bindParam(':applicationId', $applicationId, PDO::PARAM_INT);
        return $result->execute();
	}

	public static function complete($userId, $applicationId)
	{
		// Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = "UPDATE applications SET is_active = 0, end_date = NOW() WHERE applications.id = :applicationId AND master_id = :userId";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':userId', $userId, PDO::PARAM_INT);
        $result->bindParam(':applicationId', $applicationId, PDO::PARAM_INT);
        return $result->execute();
	}

	public static function getAllStatistics($userId)
	{
		$db = Db::getConnection();

		$statisticsList = array();

		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE type = 'Сервисная' AND master_id = $userId AND is_active = 0");

		$row = $result->fetch();

		$statisticsList['repair_complete'] = $row['count'];

		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE type = 'Сервисная' AND master_id = $userId AND is_active = -1");

		$row = $result->fetch();

		$statisticsList['repair_cancel'] = $row['count'];



		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE type = 'Выездная' AND master_id = $userId AND is_active = 0");

		$row = $result->fetch();

		$statisticsList['field_repair_complete'] = $row['count'];

		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE type = 'Выездная' AND master_id = $userId AND is_active = -1");

		$row = $result->fetch();

		$statisticsList['field_repair_cancel'] = $row['count'];


		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE type = 'Запрос' AND master_id = $userId AND is_active = 0");

		$row = $result->fetch();

		$statisticsList['calculation_complete'] = $row['count'];

		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE type = 'Запрос' AND master_id = $userId AND is_active = -1");

		$row = $result->fetch();

		$statisticsList['calculation_cancel'] = $row['count'];

		return $statisticsList;
	}

	public static function getStatisticsByDate($userId, $date)
	{
		$db = Db::getConnection();

		$statisticsList = array();


		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE type = 'Сервисная' AND master_id = $userId AND is_active = 0 AND end_date > '$date'");

		$row = $result->fetch();

		$statisticsList['repair_complete'] = $row['count'];



		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE type = 'Выездная' AND master_id = $userId AND is_active = 0 AND end_date > '$date'");

		$row = $result->fetch();

		$statisticsList['field_repair_complete'] = $row['count'];



		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE type = 'Запрос' AND master_id = $userId AND is_active = 0 AND end_date > '$date'");

		$row = $result->fetch();

		$statisticsList['calculation_complete'] = $row['count'];


		$result = $db->query("SELECT COUNT(*) AS count FROM applications WHERE master_id = $userId AND is_active = -1 AND end_date > '$date'");

		$row = $result->fetch();

		$statisticsList['cancel'] = $row['count'];

		return $statisticsList;
	}

	public static function getArchive($userId, $template = '')
	{
		// Соединение с БД
        $db = Db::getConnection();
        
        $applicationsList = array();

		if($template != ''){
			$result = $db->query("SELECT * FROM repair INNER JOIN applications ON applications.application_id = repair.id
        	WHERE about LIKE '%$template%' OR producer LIKE '%$template%' OR model LIKE '%$template%' OR report LIKE '%$template%' 
        		AND applications.master_id = $userId AND (is_active=0 OR is_active=-1) ORDER BY end_date ASC");
		}
		else {
			$result = $db->query("SELECT * FROM repair INNER JOIN applications ON applications.application_id = repair.id 
        	WHERE applications.master_id = $userId AND is_active=0 OR is_active=-1 ORDER BY end_date ASC");
		}

        $i = 0;

		while ($row = $result->fetch()) {
		 	$applicationsList[$i]['id'] = $row['id'];
		 	$applicationsList[$i]['about'] = $row['about'];
		 	$applicationsList[$i]['producer'] = $row['producer'];
		 	$applicationsList[$i]['model'] = $row['model'];
			$applicationsList[$i]['serial'] = $row['serial'];
		 	$applicationsList[$i]['date'] = $row['date'];
		 	$applicationsList[$i]['end_date'] = $row['end_date'];
		 	$applicationsList[$i]['report'] = $row['report'];
		 	$applicationsList[$i]['type'] = 'Сервисная';
		 	$i++;
		 }

		if($template != ''){
			$result = $db->query("SELECT * FROM field_repair INNER JOIN applications ON applications.application_id = field_repair.id 
				WHERE about LIKE '%$template%' OR producer LIKE '%$template%' OR model LIKE '%$template%' OR report LIKE '%$template%'
        			AND applications.master_id = $userId AND (is_active=0 OR is_active=-1) ORDER BY end_date ASC");
		}
		else {
		    $result = $db->query("SELECT * FROM field_repair INNER JOIN applications ON applications.application_id = field_repair.id 
        	WHERE applications.master_id = $userId AND is_active=0 OR is_active=-1 ORDER BY end_date ASC");	
		}

		while ($row = $result->fetch()) {
		 	$applicationsList[$i]['id'] = $row['id'];
		 	$applicationsList[$i]['about'] = $row['about'];
		 	$applicationsList[$i]['producer'] = $row['producer'];
		 	$applicationsList[$i]['model'] = $row['model'];
			$applicationsList[$i]['serial'] = $row['serial'];
		 	$applicationsList[$i]['date'] = $row['date'];
		 	$applicationsList[$i]['end_date'] = $row['end_date'];
		 	$applicationsList[$i]['report'] = $row['report'];
		 	$applicationsList[$i]['type'] = 'Выездная';
		 	$i++;
		}

		if($template != ''){
			$result = $db->query("SELECT * FROM calculation INNER JOIN applications ON applications.application_id = calculation.id 
        	WHERE about LIKE '%$template%' OR producer LIKE '%$template%' OR model LIKE '%$template%' OR report LIKE '%$template%'
        		AND applications.master_id = $userId AND (is_active=0 OR is_active=-1) ORDER BY end_date ASC");
		}
		else {
			$result = $db->query("SELECT * FROM calculation INNER JOIN applications ON applications.application_id = calculation.id 
        	WHERE applications.master_id = $userId AND is_active=0 OR is_active=-1 ORDER BY end_date ASC");
		}        

		while ($row = $result->fetch()) {
		 	$applicationsList[$i]['id'] = $row['id'];
		 	$applicationsList[$i]['about'] = $row['about'];
		 	$applicationsList[$i]['producer'] = $row['producer'];
		 	$applicationsList[$i]['model'] = $row['model'];
			$applicationsList[$i]['serial'] = $row['serial'];
		 	$applicationsList[$i]['date'] = $row['date'];
		 	$applicationsList[$i]['end_date'] = $row['end_date'];
		 	$applicationsList[$i]['report'] = $row['report'];
		 	$applicationsList[$i]['type'] = 'Запрос';
		 	$i++;
		 }

        return  $applicationsList;
	}
}