<?php

class Application
{
    public static function getApplicationAjax($id, $serial)
    {
        $db = Db::getConnection();

        $application = array();

        // Текст запроса к БД Сервисная
        $result = $db->query("SELECT status, price FROM repair INNER JOIN applications ON applications.application_id = repair.id WHERE applications.id = $id AND serial = '$serial'");

        while ($row = $result->fetch()) {
            $application['status'] = $row['status'];
            $application['price'] = $row['price'];
        }

        // Текст запроса к БД Выездная
        $result = $db->query("SELECT status, price FROM field_repair INNER JOIN applications ON applications.application_id = field_repair.id WHERE applications.id = $id AND serial = '$serial'");

        while ($row = $result->fetch()) {
            $application['status'] = $row['status'];
            $application['price'] = $row['price'];
        }         

        // Текст запроса к БД Запрос
        $result = $db->query("SELECT status, price FROM calculation INNER JOIN applications ON applications.application_id = calculation.id WHERE applications.id = $id AND serial = '$serial'");

        while ($row = $result->fetch()) {
            $application['status'] = $row['status'];
            $application['price'] = $row['price'];
         }

        return  $application;
    }

	public static function addRepair($name, $email, $phone, $producer, $model, $serial, $is_urgently, $is_delivery, $post_service, $street, $home, $office, $about, $additionally, $user_id, $price = 0, $status = 'Ожидает')
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = 'INSERT INTO repair (name, email, phone, producer, model, serial, is_urgently, is_delivery, post_service, street, home, office, about, additionally, user_id, price, status) '
                . 'VALUES (:name, :email, :phone, :producer, :model, :serial, :is_urgently, :is_delivery, :post_service, :street, :home, :office, :about, :additionally, :user_id, :price, :status)';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':phone', $phone, PDO::PARAM_STR);
        $result->bindParam(':producer', $producer, PDO::PARAM_STR);
        $result->bindParam(':model', $model, PDO::PARAM_STR);
        $result->bindParam(':serial', $serial, PDO::PARAM_STR);
        $result->bindParam(':is_urgently', $is_urgently, PDO::PARAM_INT);
        $result->bindParam(':is_delivery', $is_delivery, PDO::PARAM_INT);
        $result->bindParam(':post_service', $post_service, PDO::PARAM_STR);
        $result->bindParam(':street', $street, PDO::PARAM_STR);
        $result->bindParam(':home', $home, PDO::PARAM_STR);
        $result->bindParam(':office', $office, PDO::PARAM_STR);
        $result->bindParam(':about', $about, PDO::PARAM_STR);
        $result->bindParam(':additionally', $additionally, PDO::PARAM_STR);
        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $result->bindParam(':price', $price, PDO::PARAM_INT);
        $result->bindParam(':status', $status, PDO::PARAM_STR);

        
        if ($result->execute()) {
            // Если запрос выполенен успешно, возвращаем id добавленной записи, а также добавлем запись о заказе
            $sql = "INSERT INTO applications (type, application_id) VALUES ('Сервисная', ".$db->lastInsertId().")";
            $result = $db->prepare($sql);
            $result->execute();

            return $db->lastInsertId();
        }
        // Иначе возвращаем 0
        return 0;
    }

	public static function addField($name, $email, $phone, $street, $home, $office, $producer, $model, $serial, $about, $is_field, $is_urgently, $user_id, $price = 0, $status = 'Ожидает')
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = 'INSERT INTO field_repair (name, email, phone, street, home, office, producer, model, serial, about, is_field, is_urgently, user_id, price, status) '
                . 'VALUES (:name, :email, :phone, :street, :home, :office, :producer, :model, :serial, :about, :is_field, :is_urgently, :user_id, :price, :status)';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':phone', $phone, PDO::PARAM_STR);
        $result->bindParam(':street', $street, PDO::PARAM_STR);
        $result->bindParam(':home', $home, PDO::PARAM_STR);
        $result->bindParam(':office', $office, PDO::PARAM_STR);
        $result->bindParam(':producer', $producer, PDO::PARAM_STR);
        $result->bindParam(':model', $model, PDO::PARAM_STR);
        $result->bindParam(':serial', $serial, PDO::PARAM_STR);
		$result->bindParam(':about', $about, PDO::PARAM_STR);
        $result->bindParam(':is_field', $is_field, PDO::PARAM_INT);
        $result->bindParam(':is_urgently', $is_urgently, PDO::PARAM_INT);
        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $result->bindParam(':price', $price, PDO::PARAM_INT);
        $result->bindParam(':status', $status, PDO::PARAM_STR);

        
        if ($result->execute()) {
            // Если запрос выполенен успешно, возвращаем id добавленной записи, а также добавлем запись о заказе
            $sql = "INSERT INTO applications (type, application_id) VALUES ('Выездная', ".$db->lastInsertId().")";
            $result = $db->prepare($sql);
            $result->execute();

            return $db->lastInsertId();
        }
        // Иначе возвращаем 0
        return 0;
    }

    public static function addCalculation($name, $email, $phone, $producer, $model, $serial, $about, $is_mail_answer, $user_id, $status = 'Ожидает')
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = 'INSERT INTO calculation (name, email, phone, producer, model, serial, about, is_mail_answer, user_id, status) '
                . 'VALUES (:name, :email, :phone, :producer, :model, :serial, :about, :is_mail_answer, :user_id, :status)';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':phone', $phone, PDO::PARAM_STR);
        $result->bindParam(':producer', $producer, PDO::PARAM_STR);
        $result->bindParam(':model', $model, PDO::PARAM_STR);
        $result->bindParam(':serial', $serial, PDO::PARAM_STR);
		$result->bindParam(':about', $about, PDO::PARAM_STR);
        $result->bindParam(':is_mail_answer', $is_mail_answer, PDO::PARAM_INT);
        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $result->bindParam(':status', $status, PDO::PARAM_STR);

        
        if ($result->execute()) {
            // Если запрос выполенен успешно, возвращаем id добавленной записи, а также добавлем запись о заказе
            $sql = "INSERT INTO applications (type, application_id) VALUES ('Запрос', ".$db->lastInsertId().")";
            $result = $db->prepare($sql);
            $result->execute();

            return $db->lastInsertId();
        }
        // Иначе возвращаем 0
        return 0;
    }

	public static function getAdditionPriceList()
	{
		$db = Db::getConnection();

		$result = array();

		$result = $db->query('SELECT name, price FROM prices_addition')->fetchAll(PDO::FETCH_KEY_PAIR);

		return $result;
	}
	
	public static function checkFill($parameter)
    {
        if (mb_strlen($parameter, 'utf8') >= 2) {
            return true;
        }
        return false;
    }

    public static function getApplicationPic($id)
    {
        // Название изображения-пустышки
        $noImage = 'no-img.png';

        // Путь к папке с фотографиями
        $path = '/data/applications/';

        // Путь к изображению
        $pathToProductImage = $path . $id . '.jpg';

        if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToProductImage)) {
            // Если изображение пользователя существует
            // Возвращаем путь изображения
            return $pathToProductImage;
        }

        // Возвращаем путь изображения-пустышки
        return '/data/applications/' . $noImage;
    }

}
