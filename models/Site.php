<?php

class Site
{

	public static function getNewsList()
	{
		$db = Db::getConnection();

		$siteNewsList = array();

		$result = $db->query('SELECT * FROM news ORDER BY date DESC LIMIT 3');

		$i = 0;
		while ($row = $result->fetch()) {
		 	$siteNewsList[$i]['id'] = $row['id'];
		 	$siteNewsList[$i]['title'] = $row['title'];
		 	$siteNewsList[$i]['text'] = $row['text'];
		 	$siteNewsList[$i]['date'] = $row['date'];
		 	$i++;
		 }

		 return $siteNewsList;
	}

}