<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Verfix</title>

	<!-- Title ICO -->
	<link rel="shortcut icon" href="/template/img/logo.png" type="image/x-icon">

	<!-- Bootstrap Files -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

	<!-- StyleSheets Files -->
	<link rel="stylesheet" href="/template/css/main.css">
</head>
<body>
	
	<!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
		<a class="navbar-brand" href="/">
			<img src="/template/img/logo.png" style="width:40px;" class="d-inline-block align-top" alt="">
			Verfix
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    	<span class="navbar-toggler-icon"></span>
	  	</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto text-center">
				<li class="nav-item">
					<a class="nav-link" href="/">Главная</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/news">Новости</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/prices">Цены</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Заявка
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="/repair">Заявка на ремонт</a>
						<a class="dropdown-item" href="/field_repair">Выездной ремонт</a>
						<a class="dropdown-item" href="/calculation">Онлайн запрос стоимости ремонта</a>
						<div class="dropdown-divider"></div>
						<a href="" class="dropdown-item" data-toggle="modal" data-target="#CheckModal">Отследить состояние ремонта</a>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/faq">Вопросы и ответы</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/about">О компании</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/contacts">Контакты</a>
				</li>
			</ul>
			<?php if(User::isGuest()): ?>
				<div class="my-2 my-lg-0 text-center">
					<a class="btn btn-outline-success mr-sm-1" href="/login">Вход</a>
					<a class="btn btn-outline-primary" href="/register">Регистрация</a>
				</div>
			<?php else: ?>
				<?php 
					$userId = User::checkLogged();
					$user = User::getUserByID($userId);
				?>
				<div class="my-2 my-lg-0 text-center">

					<?php if($user['role'] == 'Администратор'): ?>
						<a class="btn btn-outline-danger mr-2" href="/admin" role="button">Администратор</a>
						<a class="btn btn-outline-warning mr-2" href="/master" role="button">Мастер</a>
					<?php endif; ?>

					<?php if($user['role'] == 'Мастер'): ?>
						<a class="btn btn-outline-warning mr-2" href="/master" role="button">Мастер</a>
					<?php endif; ?>

					<div class="btn-group align-items-center">
						<a role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white; cursor: pointer;">
						    <img src="<?php echo User::getProfilePic($user['id']); ?>" class="rounded-circle" style="width:40px;">
						</a>
						<div class="dropdown-menu dropdown-menu-right">
							<h6 class="dropdown-header"><?php echo $user['name']; ?></h6>
							<h6 class="dropdown-header">Статус: <?php echo $user['role']; ?></h6>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="/profile/repair">Просмотр заявок</a>
							<a class="dropdown-item" href="/profile">Настройки</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="/logout">Выход</a>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</nav>

	<!-- Modal Windows -->
	<div id="CheckModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Отследить состояние ремонта</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form method="post">
						<div class="form-group">
							<input type="text" id="CheackInputNumber" class="form-control" placeholder="Номер квитанции">
						</div>
						<div class="form-group">
							<input type="text" id="CheackInputSerial" class="form-control" placeholder="Серийный номер">
						</div>
						<div class="modal-footer">
							<button class="btn btn-secondary" data-dismiss="modal">Отмена</button>
							<button type="button" id="send" class="btn btn-primary">Найти</button>
						</div>
						<div id="ajax_answer"></div>
					</form>
				</div>
			</div>
  		</div>
	</div>

	<!-- Scroll To Top Button -->
	<button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-chevron-up"></i></button>