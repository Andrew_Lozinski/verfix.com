	<!-- Footer -->
    <footer id="myFooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <h2 class="logo"><a href="#"> VERFIX </a></h2>
                </div>
                <div class="col-sm-2">
                    <h5>Начало</h5>
                    <ul>
                        <li><a href="/">Главная</a></li>
                        <li><a href="/login">Вход</a></li>
                        <li><a href="/register">Регистрация</a></li>
                    </ul>
                </div>
                <div class="col-sm-2">
                    <h5>О нас</h5>
                    <ul>
                        <li><a href="/about">О компании</a></li>
                        <li><a href="/contacts">Контакты</a></li>
                    </ul>
                </div>
                <div class="col-sm-2">
                    <h5>Поддержка</h5>
                    <ul>
                        <li><a href="/faq">Вопросы и ответы</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <div class="social-networks">
                        <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
                        <a href="#" class="facebook"><i class="fab fa-facebook"></i></a>
                        <a href="https://t.me/verfixcom" target="_blank" class="telegram"><i class="fab fa-telegram-plane"></i></a>
                    </div>
                    <a href="/calculation" class="btn btn-default">Узнать цену</a>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <p>© 2019 Copyright Verfix. All Rights Reserved</p>
        </div>
    </footer>



	<!-- JS Files -->
	<script src="/template/js/main.js"></script>
    
    <!-- AJAX -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

	<!-- Bootstrap Files -->
	<!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#send").click(function () {
                var params = {
                    ajax_id: $("#CheackInputNumber").val(),
                    ajax_serial: $("#CheackInputSerial").val()
                }
                $.post("/application/showAjax", params, function (data) {
                    $("#ajax_answer").html(data);
                })
            });
        });
    </script>
</body>
</html>

