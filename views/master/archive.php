<?php include ROOT . '/views/layouts/header.php' ?>
	
	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3">
		<div class="container">
			<h1 class="display-4">Мастер панель</h1>
			<hr>
			<p class="lead">Приветствую, вы вошли в мастер панель!!! Будьте крайне внимательны и бдительны!</p>
		</div>
	</div>

	<!-- Master -->
	<div class="container-fluid py-3">
		<div class="row">

			<div class="col-md-12 col-lg-2">
				<div class="list-group">
					<h5 class="list-group-item list-group-item-action">Мастер меню</h5>
					<a href="/master" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Доступные заявки<span class="badge badge-primary badge-pill"><?php echo Master::getCountAvailable(); ?></span></a>
					<a href="/master/current" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Текущая заявка<i class="fas fa-clipboard"></i></a>
					<a href="/master/directions" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Порядок работы<i class="fas fa-gavel"></i></a>
					<a href="/master/statistics" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Статистика<i class="fas fa-info-circle"></i></a>
					<a href="/master/archive" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center"><i class="fas fa-caret-right"></i>Архив<i class="fas fa-archive"></i></a>
				</div>
			</div>

			<div class="col-md-12 col-lg-10">
				<div class="col-12 text-muted"><h2>Архив</h2><hr></div>
				<div class="col-12">
					
					<form class="form-inline my-2 my-lg-0" method="post">
					      <input class="form-control mr-sm-2" type="search" placeholder="Search" name="template">
					      <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="submit">Поиск</button>
					</form>
		
					<hr>

				<table class="table text-center">
					<thead class="thead-light table-sm">
						<tr>
							<th scope="col">#</th>
							<th scope="col">Неисправность</th>
							<th scope="col">Производитель</th>
							<th scope="col">Модель</th>
							<th scope="col">Серийный номер</th>
							<th scope="col">Дата заявки</th>
							<th scope="col">Дата окончание</th>
							<th scope="col">Тип</th>
							<th scope="col">Рапорт</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($applicationsList as $applicationItem): ?>
						<tr>
							<th scope="row"><?php echo $applicationItem['id']; ?></th>
							<td><?php echo $applicationItem['about']; ?></td>
							<td><?php echo $applicationItem['producer']; ?></td>
							<td><?php echo $applicationItem['model']; ?></td>
							<td><?php echo $applicationItem['serial']; ?></td>
							<td><?php echo $applicationItem['date']; ?></td>
							<td><?php echo $applicationItem['end_date']; ?></td>
							<td>
								<?php if($applicationItem['type'] == 'Сервисная'): ?>
									<span class="badge badge-success">Сервисная</span>
								<?php elseif($applicationItem['type'] == 'Выездная'): ?>
									<span class="badge badge-warning">Выездная</span>
								<?php else: ?>
									<span class="badge badge-danger">Запрос</span>
								<?php endif; ?>
							</td>
							<td><?php echo $applicationItem['report']; ?></td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

				</div>
			</div>

		</div>
	</div>

<?php include ROOT . '/views/layouts/footer.php' ?>