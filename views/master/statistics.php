<?php include ROOT . '/views/layouts/header.php' ?>
	
	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3">
		<div class="container">
			<h1 class="display-4">Мастер панель</h1>
			<hr>
			<p class="lead">Приветствую, вы вошли в мастер панель!!! Будьте крайне внимательны и бдительны!</p>
		</div>
	</div>

	<!-- Master -->
	<div class="container-fluid py-3">
		<div class="row">

			<div class="col-md-12 col-lg-2">
				<div class="list-group">
					<h5 class="list-group-item list-group-item-action">Мастер меню</h5>
					<a href="/master" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Доступные заявки<span class="badge badge-primary badge-pill"><?php echo Master::getCountAvailable(); ?></span></a>
					<a href="/master/current" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Текущая заявка<i class="fas fa-clipboard"></i></a>
					<a href="/master/directions" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Порядок работы<i class="fas fa-gavel"></i></a>
					<a href="/master/statistics" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center"><i class="fas fa-caret-right"></i>Статистика<i class="fas fa-info-circle"></i></a>
					<a href="/master/archive" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Архив<i class="fas fa-archive"></i></a>
				</div>
			</div>

			<div class="col-md-12 col-lg-10">
				<div class="col-12 text-muted"><h2>Статистика</h2><hr></div>

				<div class="row">

					<div class="col-md-12 col-lg-6">
						
						<div class="row text-left align-items-center">
							<div class="col-3">
								<img src="<?php echo User::getProfilePic($user['id']); ?>" width="160" height="160" class="img-thumbnail mb-3">
							</div>
							<div class="col-9 text-muted">
								<h2><?php echo $user['name']; ?></h2>
								<p>Статус: <?php echo $user['role']; ?></p>
							</div>
						</div>
						

						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего завершенных сервисных заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statisticsFull['repair_complete']; ?>"> 
						</div>
						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего отклоненных сервисных заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statisticsFull['repair_cancel']; ?>"> 
						</div>

						<hr>

						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего завершенных выездных заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statisticsFull['field_repair_complete']; ?>">
						</div>
						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего отклоненных выездных заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statisticsFull['field_repair_cancel']; ?>"> 
						</div>

						<hr>

						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего завершенных запрос-заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statisticsFull['calculation_complete']; ?>">
						</div>
						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего отклоненных запрос-заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statisticsFull['calculation_cancel']; ?>"> 
						</div>

					</div>

										<div class="col-md-12 col-lg-6">
						<form class="form-inline my-2 my-lg-0" method="post">
							<select class="form-control mr-sm-2" name="date">
								<option selected disabled>Выберите период...</option>
								<option value="1">За день</option>
								<option value="2">За месяц</option>
								<option value="3">За квартал</option>
								<option value="4">За год</option>
							</select>
						    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name = "submit">Поиск</button>
						</form>

						<div class="alert alert-secondary my-3" role="alert">
							Статистика <?php echo $interval; ?><hr>
							<p class="text-muted">с <?php echo $date; ?> по <?php echo date('Y-m-d'); ?></p>
						</div>

						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего завершенных сервисных заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statistics['repair_complete']; ?>">
						</div>

						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего завершенных выездных заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statistics['field_repair_complete']; ?>">
						</div>

						<div class="input-group mb-5">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего завершенных запрос-заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statistics['calculation_complete']; ?>">
						</div>

						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего отклоненных заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statistics['cancel']; ?>">
						</div>
						
					</div>

				</div>


			</div>

		</div>
	</div>

<?php include ROOT . '/views/layouts/footer.php' ?>