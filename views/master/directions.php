<?php include ROOT . '/views/layouts/header.php' ?>
	
	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3">
		<div class="container">
			<h1 class="display-4">Мастер панель</h1>
			<hr>
			<p class="lead">Приветствую, вы вошли в мастер панель!!! Будьте крайне внимательны и бдительны!</p>
		</div>
	</div>

	<!-- Master -->
	<div class="container-fluid py-3">
		<div class="row">

			<div class="col-md-12 col-lg-2">
				<div class="list-group">
					<h5 class="list-group-item list-group-item-action">Мастер меню</h5>
					<a href="/master" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Доступные заявки<span class="badge badge-primary badge-pill"><?php echo Master::getCountAvailable(); ?></span></a>
					<a href="/master/current" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Текущая заявка<i class="fas fa-clipboard"></i></a>
					<a href="/master/directions" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center"><i class="fas fa-caret-right"></i>Порядок работы<i class="fas fa-gavel"></i></a>
					<a href="/master/statistics" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Статистика<i class="fas fa-info-circle"></i></a>
					<a href="/master/archive" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Архив<i class="fas fa-archive"></i></a>
				</div>
			</div>

			<div class="col-md-12 col-lg-10">
				<div class="col-12 text-muted"><h2>Порядок работы</h2><hr></div>
				<div class="col-12">
					
					<?php $i = 1; ?>
					<?php foreach ($directionsList as $directionsItem): ?>
						<p class="text-muted text-right small m-0"><?php echo $directionsItem['date']; ?></p>
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text"><?php echo $i++; ?></span>
								<span class="input-group-text"><?php echo $directionsItem['title']; ?></span>
							</div>
							<textarea class="form-control" aria-label="With textarea"><?php echo $directionsItem['text']; ?></textarea>
						</div>
					<?php endforeach; ?>

				</div>
			</div>

		</div>
	</div>

<?php include ROOT . '/views/layouts/footer.php' ?>