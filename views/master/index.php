<?php include ROOT . '/views/layouts/header.php' ?>
	
	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3">
		<div class="container">
			<h1 class="display-4">Мастер панель</h1>
			<hr>
			<p class="lead">Приветствую, вы вошли в мастер панель!!! Будьте крайне внимательны и бдительны!</p>
		</div>
	</div>

	<!-- Master -->
	<div class="container-fluid py-3">
		<div class="row">
			
			<div class="col-md-12 col-lg-2">
				<div class="list-group">
					<h5 class="list-group-item list-group-item-action">Мастер меню</h5>
					<a href="/master" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center"><i class="fas fa-caret-right"></i>Доступные заявки<span class="badge badge-primary badge-pill"><?php echo Master::getCountAvailable(); ?></span></a>
					<a href="/master/current" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Текущая заявка<i class="fas fa-clipboard"></i></a>
					<a href="/master/directions" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Порядок работы<i class="fas fa-gavel"></i></a>
					<a href="/master/statistics" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Статистика<i class="fas fa-info-circle"></i></a>
					<a href="/master/archive" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Архив<i class="fas fa-archive"></i></a>
				</div>
			</div>

			<div class="col-md-12 col-lg-10">
				<div class="alert alert-secondary m-0 text-center" role="alert">
					<h4 class="alert-heading">Ожидают</h4>
				</div>
				<table class="table text-center">
					<thead class="thead-light table-sm">
						<tr>
							<th scope="col">#</th>
							<th scope="col">Неисправность</th>
							<th scope="col">Производитель</th>
							<th scope="col">Модель</th>
							<th scope="col">Серийный номер</th>
							<th scope="col">Дата</th>
							<th scope="col">Тип</th>
							<th scope="col">Дополнительно</th>
							<th scope="col">Принять</th>
							<th scope="col">Отклонить</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1; ?>
						<?php foreach ($availableApplicationsList as $availableApplicationsItem): ?>
							<tr>
								<th scope="row"><?php echo $i++; ?></th>
								<td><?php echo $availableApplicationsItem['about']; ?></td>
								<td><?php echo $availableApplicationsItem['producer']; ?></td>
								<td><?php echo $availableApplicationsItem['model']; ?></td>
								<td><?php echo $availableApplicationsItem['serial']; ?></td>
								<td><?php echo $availableApplicationsItem['date']; ?></td>
								<td>
									<?php if($availableApplicationsItem['type'] == 'Сервисная'): ?>
										<span class="badge badge-success">Сервисная</span>
									<?php elseif($availableApplicationsItem['type'] == 'Выездная'): ?>
										<span class="badge badge-warning">Выездная</span>
									<?php else: ?>
										<span class="badge badge-danger">Запрос</span>
									<?php endif; ?>
								</td>
								<td>
									<?php if($availableApplicationsItem['is_urgently']): ?>
										<span class="badge badge-primary">Срочная</span>
									<?php endif; ?>

									<?php if($availableApplicationsItem['is_field']): ?>
										<span class="badge badge-primary">Выезд на место</span>
									<?php endif; ?>

									<?php if($availableApplicationsItem['is_delivery']): ?>
										<span class="badge badge-primary">Доставка</span>
									<?php endif; ?>

									<?php if($availableApplicationsItem['is_mail_answer']): ?>
										<span class="badge badge-primary">Ответ в почту</span>
									<?php endif; ?>
								</td>
								<td>
									<button type="button" class = "btn btn-success btn-sm" data-toggle="modal" data-target="#modalConfirm" id="<?php echo $availableApplicationsItem['id']; ?>">
										<i class="fas fa-check"></i>
									</button>
								</td>
								<td>
									<button type="button" class = "btn btn-danger btn-sm" data-toggle="modal" data-target="#modalDelete" id="<?php echo $availableApplicationsItem['id']; ?>">
										<i class="fas fa-times"></i>
									</button>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>

		</div>
	</div>

	<!-- Modals -->
	<div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Подтверждение</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Вы действительно хотите принять эту заявку?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
					<a href="" id="confirm" class="btn btn-success">Принять</a>
				</div>
			</div>
		</div>
	</div>



	<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Удаление</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Вы действительно хотите отклонить эту заявку?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
					<a href="" id="cancel" class="btn btn-danger">Отклонить</a>
				</div>
			</div>
		</div>
	</div>

	<?php if(isset($_SESSION['error'])): ?>
		<div class="modal fade" id="modalErrror" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Ошибка</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p class="my-auto"><?php echo $_SESSION['error']; ?></p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Ок</button>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>


<?php include ROOT . '/views/layouts/footer.php' ?>

	<script>
        $('#modalConfirm').on('show.bs.modal', function(e) {
        	$("#confirm").attr('href', '/master/confirm/' + e.relatedTarget.id);    
        })

        $('#modalDelete').on('show.bs.modal', function(e) {
        	$("#cancel").attr('href', '/master/cancel/' + e.relatedTarget.id);    
        })
    </script>

	<?php if(isset($_SESSION['error'])): ?>
	    <script>
	    	$("#modalErrror").modal()
	    </script>
    <?php endif; unset($_SESSION["error"]); ?>