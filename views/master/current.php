<?php include ROOT . '/views/layouts/header.php' ?>
	
	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3">
		<div class="container">
			<h1 class="display-4">Мастер панель</h1>
			<hr>
			<p class="lead">Приветствую, вы вошли в мастер панель!!! Будьте крайне внимательны и бдительны!</p>
		</div>
	</div>

	<!-- Master -->
	<div class="container-fluid py-3">
		<div class="row">

			<div class="col-md-12 col-lg-2">
				<div class="list-group">
					<h5 class="list-group-item list-group-item-action">Мастер меню</h5>
					<a href="/master" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Доступные заявки<span class="badge badge-primary badge-pill"><?php echo Master::getCountAvailable(); ?></span></a>
					<a href="/master/current" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center"><i class="fas fa-caret-right"></i>Текущая заявка<i class="fas fa-clipboard"></i></a>
					<a href="/master/directions" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Порядок работы<i class="fas fa-gavel"></i></a>
					<a href="/master/statistics" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Статистика<i class="fas fa-info-circle"></i></a>
					<a href="/master/archive" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Архив<i class="fas fa-archive"></i></a>
				</div>
			</div>
			<?php if($applicationItem == ''): ?>
				<div class="col-md-12 col-lg-10">
					<h2 class="text-muted">Нет заявки. Выберите свободную заявку.</h2>
				</div>
			<?php else: ?>
			<div class="row col-md-12 col-lg-10">
				<div class="col-12 text-muted"><h2>Заявка № <?php echo $applicationItem['id']; ?></h2><hr></div>
				<div class="col-md-12 col-lg-6">
					<h3 class="text-muted">Информация пользователя</h3>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 200px;">Тип</span>
						</div>
						<input type="text" class="form-control" value="<?php echo $applicationItem['type']; ?>">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 200px;">Дата</span>
						</div>
						<input type="text" class="form-control" value="<?php echo $applicationItem['date']; ?>"> 
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 200px;">ФИО</span>
						</div>
						<input type="text" class="form-control" value="<?php echo $applicationItem['name']; ?>">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 200px;">Телефон</span>
						</div>
						<input type="text" class="form-control" value="<?php echo $applicationItem['phone']; ?>">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 200px;">Почта</span>
						</div>
						<input type="text" class="form-control" value="<?php echo $applicationItem['email']; ?>">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 200px;">Производитель</span>
						</div>
						<input type="text" class="form-control" value="<?php echo $applicationItem['producer']; ?>">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 200px;">Модель</span>
						</div>
						<input type="text" class="form-control" value="<?php echo $applicationItem['model']; ?>">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 200px;">Серийный номер</span>
						</div>
						<input type="text" class="form-control" value="<?php echo $applicationItem['serial']; ?>">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 250px;">Неисправность</span>
						</div>
						<textarea class="form-control" rows="3"><?php echo $applicationItem['about']; ?></textarea>
					</div>

					<?php if(isset($applicationItem['additionally'])): ?>
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text" style="min-width: 250px;">Пароли, важная информация</span>
							</div>
							<textarea class="form-control" rows="3"><?php echo $applicationItem['additionally']; ?></textarea>
						</div>
					<?php endif; ?>

					<div class="mb-3">
						<div>
							<span class="input-group-text" style="min-width: 250px;">Фото</span>
						</div>
						<img src="<?php echo Application::getApplicationPic($applicationItem['id']); ?>" width="100%" height="100%" class="img-thumbnail">
					</div>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 200px;">Дополнительно</span>
						</div>
						<?php if(isset($applicationItem['is_urgently']) && $applicationItem['is_urgently']): ?>
							<span class="badge badge-primary my-auto ml-2">Срочная</span>
						<?php endif; ?>

						<?php if(isset($applicationItem['is_field']) && $applicationItem['is_field']): ?>
							<span class="badge badge-primary my-auto ml-2">Выезд на место</span>
						<?php endif; ?>

						<?php if(isset($applicationItem['is_delivery']) && $applicationItem['is_delivery']): ?>
							<span class="badge badge-primary my-auto ml-2">Доставка</span>
						<?php endif; ?>

						<?php if(isset($applicationItem['is_mail_answer']) && $applicationItem['is_mail_answer']): ?>
							<span class="badge badge-primary my-auto ml-2">Ответ в почту</span>
						<?php endif; ?>
					</div>
					
					<?php if(isset($applicationItem['is_delivery']) && $applicationItem['is_delivery']): ?>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 200px;">Почтовый сервис</span>
						</div>
						<input type="text" class="form-control" value="<?php echo $applicationItem['post_service']; ?>">
					</div>
					
					<?php endif; ?>
					
					<?php if((isset($applicationItem['is_delivery']) && $applicationItem['is_delivery']) || (isset($applicationItem['is_field']) && $applicationItem['is_field'])): ?>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 200px;">Улица</span>
						</div>
						<input type="text" class="form-control" value="<?php echo $applicationItem['street']; ?>">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 200px;">Номер дома</span>
						</div>
						<input type="text" class="form-control" value="<?php echo $applicationItem['home']; ?>">
					</div>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 200px;">Офис, квартира</span>
						</div>
						<input type="text" class="form-control" value="<?php echo $applicationItem['office']; ?>">
					</div>
					
					<?php endif; ?>

				</div>

				<div class="col-md-12 col-lg-6">
					<h3 class="text-muted">Ваша информация</h3>
					<?php if($result): ?>
						<div class="alert alert-success" role="alert">
							<strong>Сохранено!</strong>
						</div>
					<?php else: ?>
						<?php if(isset($errors) && is_array($errors)): ?>
							<?php foreach ($errors as $error): ?>
								<div class="alert alert-danger" role="alert">
									<strong>Ошибка!</strong> <?php echo $error; ?>
								</div>
							<?php endforeach; ?>
						<?php endif; ?>
					<?php endif; ?>
					<form method="post">
						<?php if($type == 'repair'): ?>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text" style="min-width: 150px;">Статус</span>
								</div>
								<select name="status" class="form-control">
									<option <?php if($status == 'Ожидает') echo "selected"; ?>>Ожидает</option>
									<option <?php if($status == 'Доставка в производственный центр') echo "selected"; ?>>Доставка в производственный центр</option>
									<option <?php if($applicationItem['status'] == 'Диагностика неисправности') echo "selected"; ?>>Диагностика неисправности</option>
									<option <?php if($status == 'Просчет стоимости') echo "selected"; ?>>Просчет стоимости</option>
									<option <?php if($status == 'Выполнение ремонта') echo "selected"; ?>>Выполнение ремонта</option>
									<option <?php if($status == 'Ремонт завершен') echo "selected"; ?>>Ремонт завершен</option>
									<option <?php if($status == 'Финальное тестирование') echo "selected"; ?>>Финальное тестирование</option>
									<option <?php if($status == 'Устройство готово к выдаче, отправка уведомления для заказчика') echo "selected"; ?>>Устройство готово к выдаче, отправка уведомления для заказчика</option>
									<option <?php if($status == 'Заказчик уведомлен') echo "selected"; ?>>Заказчик уведомлен</option>
									<option <?php if($status == 'Устройство выдано заказчику') echo "selected"; ?>>Устройство выдано заказчику</option>
								</select>
							</div>
						<?php elseif($type == 'field_repair'): ?>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text" style="min-width: 150px;">Статус</span>
								</div>
								<select name="status" class="form-control">
									<option <?php if($status == 'Ожидает') echo "selected"; ?>>Ожидает</option>
									<option <?php if($status == 'Согласование выезда') echo "selected"; ?>>Согласование выезда</option>
									<option <?php if($status == 'Выезд на место') echo "selected"; ?>>Выезд на место</option>
									<option <?php if($status == 'Диагностика неисправности') echo "selected"; ?>>Диагностика неисправности</option>
									<option <?php if($status == 'Просчет стоимости') echo "selected"; ?>>Просчет стоимости</option>
									<option <?php if($status == 'Выполнение ремонта') echo "selected"; ?>>Выполнение ремонта</option>
									<option <?php if($status == 'Ремонт завершен') echo "selected"; ?>>Ремонт завершен</option>
									<option <?php if($status == 'Устройство готово к эксплуатации, отправка уведомления для заказчика') echo "selected"; ?>>Устройство готово к эксплуатации, отправка уведомления для заказчика</option>
									<option <?php if($status == 'Устройство выдано заказчику') echo "selected"; ?>>Устройство выдано заказчику</option>
								</select>
							</div>
						<?php else: ?>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text" style="min-width: 150px;">Статус</span>
								</div>
								<select name="status" class="form-control">
									<option <?php if($status == 'Ожидает') echo "selected"; ?>>Ожидает</option>
									<option <?php if($status == 'Диагностика неисправности') echo "selected"; ?>>Диагностика неисправности</option>
									<option <?php if($status == 'Просчет стоимости') echo "selected"; ?>>Просчет стоимости</option>
									<option <?php if($status == 'Расчет стоимости подведен, отправка уведомления для заказчика') echo "selected"; ?>>Расчет стоимости подведен, отправка уведомления для заказчика</option>
									<option <?php if($status == 'Информация выдана заказчику') echo "selected"; ?>>Информация выдана заказчику</option>
								</select>
							</div>
						<?php endif; ?>

						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text" style="min-width: 150px;">Цена</span>
								<span class="input-group-text">грн.</span>
							</div>
							<input type="text" name="price" class="form-control" value="<?php echo $price; ?>"> 
						</div>
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text" style="min-width: 150px;">Рапорт</span>
							</div>
							<textarea class="form-control" name="report" rows="4" placeholder="Сюда вписывайте ваши действия..."><?php echo $report; ?></textarea>
						</div>
						
						<div class="text-right mb-3">
							<button type="submit" name="submit" class="btn btn-outline-primary">Сохранить</button>
						</div>
					</form>

					<div class="text-right mb-3">
						<button class="btn btn-outline-warning" data-toggle="modal" data-target="#modalIgnore" id="<?php echo $applicationItem['id']; ?>">Отказаться от заявки</button>
					</div>
					<div class="text-right">
						<button class="btn btn-outline-danger" data-toggle="modal" data-target="#modalCancel" id="<?php echo $applicationItem['id']; ?>">Отклонить</button>
						<button class="btn btn-outline-success" data-toggle="modal" data-target="#modalComplete" id="<?php echo $applicationItem['id']; ?>">Завершить</button>
					</div>
				
				</div>

			</div>
			<?php endif; ?>
		</div>
	</div>

	<!-- Modals -->
	<div class="modal fade" id="modalIgnore" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Подтверждение</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Вы действительно хотите отказаться от данной заявки? После отказа, заявка снова будет доступна в общем списке.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
					<a href="" id="confirm" class="btn btn-warning">Отказаться</a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalCancel" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Подтверждение</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Вы действительно хотите отклонить эту заявку?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
					<a href="" id="cancel" class="btn btn-danger">Отклонить</a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalComplete" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Подтверждение</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Вы действительно хотите завершить эту заявку?</p>
					<p>Статус <span class="input-group-text"><?php echo $status; ?></span></p>
					<p>Цена <span class="input-group-text"><?php echo $price; ?> грн.</span></p>
					<p>Рапорт <span class="input-group-text"><?php echo $report; ?></span></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
					<a href="" id="complete" class="btn btn-success">Завершить</a>
				</div>
			</div>
		</div>
	</div>

<?php include ROOT . '/views/layouts/footer.php' ?>

	<script>
        $('#modalIgnore').on('show.bs.modal', function(e) {
        	$("#confirm").attr('href', '/master/ignore/' + e.relatedTarget.id);    
        })

        $('#modalCancel').on('show.bs.modal', function(e) {
        	$("#cancel").attr('href', '/master/currentCancel/' + e.relatedTarget.id);    
        })
        $('#modalComplete').on('show.bs.modal', function(e) {
        	$("#complete").attr('href', '/master/complete/' + e.relatedTarget.id);    
        })
    </script>