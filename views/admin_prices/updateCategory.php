<?php include ROOT . '/views/layouts/header.php' ?>

	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3">
		<div class="container">
			<h1 class="display-4">Админ панель</h1>
			<hr>
			<p class="lead">Внимание, вы вошли в админ панель!!! Будьте крайне внимательны и бдительны!</p>
		</div>
	</div>

	<!-- Admin -->
	<div class="container-fluid py-3">
		<div class="row">

			<div class="col-md-12 col-lg-2">
				<div class="list-group">
					<h5 class="list-group-item list-group-item-action">Админ меню</h5>
					<a href="/admin" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Заявки<span class="badge badge-primary badge-pill"><?php echo Master::getCountAvailable(); ?></span></a>
					<a href="/admin/news" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Новости<i class="fas fa-newspaper"></i></a>
					<a href="/admin/prices" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center"><i class="fas fa-caret-right"></i>Цены<i class="fas fa-dollar-sign"></i></a>
					<a href="/admin/faq" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Вопросы и ответы<i class="fas fa-question"></i></a>
					<a href="/admin/directions" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Порядок работы<i class="fas fa-gavel"></i></a>
					<a href="/admin/post" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Отделения<i class="fas fa-building"></i></a>
					<a href="/admin/statistics" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Статистика<i class="fas fa-info-circle"></i></a>
					<a href="/admin/users" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Пользователи<i class="fas fa-users"></i></a>
				</div>
			</div>

			<div class="col-md-12 col-lg-10">
			
				<div class="col-12 text-muted"><h2>Редактировать категорию</h2><hr></div>

				<?php if(isset($errors) && is_array($errors)): ?>
					<?php foreach ($errors as $error): ?>
						<div class="alert alert-danger my-2" role="alert">
							<strong>Внимание!</strong> <?php echo $error; ?>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
				
				<div class="col-6">
					<form method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label for="name">Название</label>
							<input type="text" id="name" name="name" class="form-control" required value="<?php echo $name; ?>">
							<small class="form-text text-muted">Введите название, не меньше 5-ти символов.</small>
						</div>
						<div class="form-group">
							<label for="sort_order">Порядок сортировки</label>
							<input type="text" id="sort_order" name="sort_order" class="form-control" required value="<?php echo $sort_order; ?>">
							<small class="form-text text-muted">Введите номер порядке сортировке, чем больше это число, тем выше категория отображаеться в списке цен. По умолчанию ставьте 0.</small>
						</div>
						<div class="form-group">
							<label for="text">Статус категории</label>
							<select name="status" class="form-control">
								<option value="1" <?php if($status == 1) echo "selected"; ?>>Отображается</option>
								<option value="0" <?php if($status == 0) echo "selected"; ?>>Не отображается</option>
							</select>
							<small class="form-text text-muted">Если категория отключена, она не будет отображаться в общем списке.</small>
						</div>

						<div class="justify-content-between">
							<a class="btn btn-secondary" href="/admin/prices">Отмена</a>
							<button type="submit" name="submit" class="btn btn-success">Сохранить</button>
						</div>
					</form>
				</div>

				
			
			</div>

		</div>
	</div>


<?php include ROOT . '/views/layouts/footer.php' ?>
