<?php include ROOT . '/views/layouts/header.php' ?>
	


	<div class="p-3 p-md-5 m-md-3 text-center bg-light">
		<div class="col-md-5 p-lg-4 mx-auto my-5">
		<h1 class="display-4 font-weight-normal">VERFIX Center</h1>
		<p class="lead">В VERFIX разработан и внедрен ряд инновационных технологий – от новейшей системы коммуникации с клиентами до автоматизированного управления производством. Это малая часть преимуществ, которые выгодно отличают нас от конкурентов.</p>
		<a class="btn btn-outline-secondary" href="/contacts">Контакты</a>
		</div>
	</div>
	<div class="row">

		<div class="bg-light pt-3 text-center col-md-12 col-lg-6">
			<div class="my-3 p-3">
				<h2 class="display-5">Качество на высшем уровне</h2>
				<p class="lead">Специалисты VERFIX Center ежегодно проходят аттестацию по ремонту продукции всех обслуживаемых производителей, также посещают технические обучения и тренинги, повышая свой профессиональный уровень.</p>
			</div>
			<img src="template/img/training.jpg" class="img-thumbnail" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;">
		</div>

		<div class="bg-light pt-3 text-center col-md-12 col-lg-6">
			<div class="my-3 p-3">
				<h2 class="display-5">Высокая квалификация</h2>
				<p class="lead">Квалификацию нашей инженерной команды контролируют независимые технические аудиторы компаний-вендоров, поэтому вы можете смело обращаться с техническими вопросами любой сложности.</p>
			</div>
			<img src="template/img/qualification.jpg" class="img-thumbnail" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;">
		</div>

		<div class="bg-light py-2 text-center col-12">
			<div class="my-3 p-3">
				<h2 class="display-4">Мы ремонтируем</h2>
			</div>
			<div class="row text-center justify-content-center">
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<img src="/template/img/laptop.png" alt="" class="w-0">
					<h3>Ноутбуки</h3>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<img src="/template/img/computer-screen.png" alt="" class="w-0">
					<h3>Моноблоки</h3>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<img src="/template/img/icon.png" alt="" class="w-0">
					<h3>Планшеты</h3>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<img src="/template/img/smartphone-call.png" alt="" class="w-0">
					<h3>Смартфоны</h3>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<img src="/template/img/device.png" alt="" class="w-0">
					<h3>Видеопроекторы</h3>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<img src="/template/img/filled-speaker-with-white-details.png" alt="" class="w-0">
					<h3>Hi-Fi, AV системы</h3>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<img src="/template/img/bluray.png" alt="" class="w-0">
					<h3>CD, DVD, BRD проигрыватели</h3>
				</div>
			</div>
		</div>

      </div>
	
<?php include ROOT . '/views/layouts/footer.php' ?>