<?php include ROOT . '/views/layouts/header.php' ?>
	
	<!--Google map-->
	<div class="row">

		<div class="col-12 m-0">
			<iframe width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10206.133732941147!2d28.6369560412859!3d50.244620716343235!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x472c6493ebf252ed%3A0xc400e72454e33b55!2z0JPQvtGB0YPQtNCw0YDRgdGC0LLQtdC90L3Ri9C5INGD0L3QuNCy0LXRgNGB0LjRgtC10YIgwqvQltC40YLQvtC80LjRgNGB0LrQsNGPINC_0L7Qu9C40YLQtdGF0L3QuNC60LDCuw!5e0!3m2!1sru!2sua!4v1558769653891!5m2!1sru!2sua"></iframe>
		</div>
		<div class="container">
			<div class="row text-center justify-content-center my-5">
				<div class="col-xs-12 col-sm-4 col-lg-4">
					<img src="/template/img/maps-and-flags.png" alt="" class="w-0">
					<h3>Адрес</h3>
					<p><i class="fas fa-map-marker-alt"></i> улица Чудновская, 103, Житомир, Житомирская область, 10002</p>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-4">
					<img src="/template/img/mobile-phone.png" alt="" class="w-0">
					<h3>Контактный телефон</h3>
					<p><i class="fas fa-phone"></i> +380 093 411 44 08</p>
					<p><i class="fas fa-phone"></i> +380 093 986 05 65</p>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-4">
					<img src="/template/img/email.png" alt="" class="w-0">
					<h3>Почта</h3>
					<p><i class="fas fa-envelope"></i> andrew.lozinski.official@gmail.com</p>
				</div>
			</div>
		</div>

    </div>


	</div>
	
<?php include ROOT . '/views/layouts/footer.php' ?>