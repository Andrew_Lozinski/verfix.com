<?php include ROOT . '/views/layouts/header.php' ?>
	
	<!-- Slider -->
	<div id="demo" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ul class="carousel-indicators">
			<li data-target="#demo" data-slide-to="0" class="active"></li>
			<li data-target="#demo" data-slide-to="1"></li>
			<li data-target="#demo" data-slide-to="2"></li>
		</ul>

		<!-- The slideshow -->
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img src="/template/img/slide1.jpeg" alt="slide1" style="width:100%;height:100%;">
				<div class="carousel-caption d-none d-md-block">
					<h4>Ремонт в "Verfix Center"</h4>
					<p>Возникли проблемы с техникой? Непременно приходите к нам! Мы будем рады Вам помочь.</p>
				</div>
			</div>
			<div class="carousel-item">
				<img src="/template/img/slide2.jpeg" alt="slide2" style="width:100%;height:100%;">
				<div class="carousel-caption d-none d-md-block">
					<h4>Ремонт самых мелких деталей</h4>
					<p>Мы используем специальное оборудование которое позволяет проводить нам ремонт самых мелких деталей.</p>
				</div>
			</div>
			<div class="carousel-item">
			 	<img src="/template/img/slide3.jpeg" alt="slide3" style="width:100%;height:100%;">
				<div class="carousel-caption d-none d-md-block">
					<h5>Работа с ПО</h5>
					<p>Наш центр не ограничивается работами с физическими компонентами, мы также предлагает роботу с програмным обеспечением.</p>
				</div>
			</div>
		</div>

		<!-- Left and right controls -->
		<a class="carousel-control-prev" href="#demo" data-slide="prev">
			<span class="carousel-control-prev-icon"></span>
		</a>
		<a class="carousel-control-next" href="#demo" data-slide="next">
			<span class="carousel-control-next-icon"></span>
		</a>
	</div>

	<!-- What are we doing -->
	<div class="container-fluid py-4 bg-light">
		<div class="container p-3">
			<div class="row text-center justify-content-center">
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<img src="/template/img/speedometer.png" alt="" class="w-0">
					<h3>Скорость работы</h3>
					<p>Сотрудники компании сделают все возможное чтобы вы получили отремонтированную технику в самые короткие сроки.</p>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<img src="/template/img/medal.png" alt="" class="w-0">
					<h3>Качество</h3>
					<p>80% сотрудников работают с момента открытия, имеют достаточный опыт работы.</p>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<img src="/template/img/wallet.png" alt="" class="w-0">
					<h3>Низкая стоимость</h3>
					<p>Принцип нашей компании — высокое качество по низкой цене.</p>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<img src="/template/img/moving.png" alt="" class="w-0">
					<h3>Доставка</h3>
					<p>Мы прилагаем все усилия, чтобы вложиться в минимальные сроки по доставке прямо к вашей двери.</p>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<img src="/template/img/pin.png" alt="" class="w-0">
					<h3>Районы обхвата</h3>
					<p>Привезти технику на ремонт вы можете в самый удобный по расположению сервисный центр.</p>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<img src="/template/img/protection.png" alt="" class="w-0">
					<h3>Защита прав потребителей</h3>
					<p>Одно из направлений нашей деятельности – защита прав потребителей.</p>
				</div>
				<div class="col-xs-12 col-sm-4 col-lg-3">
					<img src="/template/img/heart.png" alt="" class="w-0">
					<h3>Любовь клиентов</h3>
					<p>Мы постоянно развиваем нашу систему и добавляем новые возможности.</p>
				</div>
			</div>
		</div>
	</div>

	<!-- News -->
	<div class="container py-4">
		<div class="card-deck">
			<?php foreach ($siteNewsList as $newsItem): ?>
			<div class="card">
				<img class="card-img-top" src="<?php echo News::getNewsPic($newsItem['id']); ?>" alt="Card image cap" style="height: 200px;">
				<div class="card-body">
					<h5 class="card-title"><?php echo $newsItem['title']; ?></h5>
					<p class="card-text"><?php echo $newsItem['text']; ?></p>
				</div>
				<div class="card-footer">
					<small class="text-muted"><?php echo $newsItem['date']; ?></small>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
	
<?php include ROOT . '/views/layouts/footer.php' ?>