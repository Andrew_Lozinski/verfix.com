<?php include ROOT . '/views/layouts/header.php' ?>

	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3">
		<div class="container">
			<h1 class="display-4">Вопросы и ответы</h1>
			<hr>
			<p class="lead">В данном разделе вы можете найти ответы на наиболее распространенние вопросы наших посетителей. В случае, если вам не удалось получить ответ на свой вопрос вы можете задать его непосредственно нашему менеджеру.</p>
		</div>
	</div>

	<!-- FAQ -->
	<div class="container-fluid bg-light">
		<div class="container pt-5">
			<div class="row">
				
				<?php foreach ($faqList as $faqItem): ?>
					<div class="col-lg-6 mb-5">
						<div class="list-group">

							<p class="list-group-item bg-info"><?php echo $faqItem['name']; ?></p>
							
							<?php $i = 1; ?>
							<?php foreach ($faqItemsList as $item): ?>
								<?php if($item['faq_name_id'] == $faqItem['id']): ?>
									<button type="button" class="list-group-item list-group-item-action" data-toggle="collapse" data-target="#answer_<?php echo $faqItem['id']; ?>_<?php echo $i; ?>"><?php echo $i; ?>. <?php echo $item['title']; ?></button>
									<div class="collapse" id="answer_<?php echo $faqItem['id']; ?>_<?php echo $i; $i++; ?>">
										<div class="card card-body">
										<?php echo $item['answer']; ?>
										</div>
									</div>
								<?php endif; ?>
							<?php endforeach; ?>

						</div>
					</div>
				<?php endforeach; ?>

			</div>
		</div>
	</div>

<?php include ROOT . '/views/layouts/footer.php' ?>