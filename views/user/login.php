<?php include ROOT . '/views/layouts/header.php' ?>
	
<div class="container-fluid bg-light">

	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3 bg-light">
		<div class="container">
			<h1 class="display-4">Вход</h1>
			<hr>
		</div>
	</div>

	<div class="container">
		<?php if(isset($errors) && is_array($errors)): ?>
			<?php foreach ($errors as $error): ?>
				<div class="alert alert-danger" role="alert">
					<strong>Внимание!</strong> <?php echo $error; ?>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>

		<form method="post">
			<div class="form-group">
				<label for="SignIninputLogin">Логин:</label>
				<input type="email" name="email" class="form-control" placeholder="Логин" value="<?php echo $email; ?>" required>
			</div>
			<div class="form-group">
				<label for="SignInInputPassword">Пароль:</label>
				<input type="password" name="password" class="form-control" placeholder="Пароль" required>
			</div>
				Нет аккаунта? <a href="/register">Зарегистрируйтесь</a>
			<div class="modal-footer">
				<button type="submit" name="submit" class="btn btn-success">Вход</button>
			</div>
		</form>
	</div>
</div>

<?php include ROOT . '/views/layouts/footer.php' ?>