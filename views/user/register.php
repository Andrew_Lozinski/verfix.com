<?php include ROOT . '/views/layouts/header.php' ?>
	
<div class="container-fluid bg-light">

	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3 bg-light">
		<div class="container">
			<h1 class="display-4">Регистрация</h1>
			<hr>
		</div>
	</div>

	<div class="container">
		<?php if(isset($errors) && is_array($errors)): ?>
			<?php foreach ($errors as $error): ?>
				<div class="alert alert-danger" role="alert">
					<strong>Внимание!</strong> <?php echo $error; ?>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>

		<form method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="name">ФИО:</label>
				<input type="text" name="name" class="form-control" placeholder="ФИО" value="<?php echo $name; ?>" required>
				<small class="form-text text-muted">Введите ваше ФИО.</small>
			</div>
			<div class="form-group">
				<label for="email">Почта:</label>
				<input type="email" name="email" class="form-control" placeholder="Почта" value="<?php echo $email; ?>" required>
				<small class="form-text text-muted">Мы никогда не передадим вашу почту сторонним лицам.</small>
			</div>
			<div class="form-group">
				<label for="phone">Номер телефона:</label>
				<input type="text" name="phone" class="form-control" placeholder="Телефон" value="<?php echo $phone; ?>" required>
				<small class="form-text text-muted">Мы никогда не передадим ваш номер телефона сторонним лицам.</small>
			</div>
			<div class="form-group">
				<label for="password_one">Пароль:</label>
				<input type="password" name="password_one" class="form-control" placeholder="Пароль" required>
				<small class="form-text text-muted">Пароль должен состоять не менее из 7 символов.</small>
			</div>
			<div class="form-group">
				<label for="password_two">Повторите пароль:</label>
				<input type="password" name="password_two" class="form-control" placeholder="Повторите пароль" required>
				<small class="form-text text-muted">Повторите ваш пароль.</small>
			</div>
			<div class="form-group">
				<label>Фотография профиля</label>
				<input type="file" name="image" class="form-control-file" />
				<small class="form-text text-muted">*не обязательно</small>
			</div>
			<div class="modal-footer">
				<button type="submit" name="submit" class="btn btn-success">Регистрация</button>
			</div>
		</form>
	</div>
</div>

<?php include ROOT . '/views/layouts/footer.php' ?>