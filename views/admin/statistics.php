<?php include ROOT . '/views/layouts/header.php' ?>

	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3">
		<div class="container">
			<h1 class="display-4">Админ панель</h1>
			<hr>
			<p class="lead">Внимание, вы вошли в админ панель!!! Будьте крайне внимательны и бдительны!</p>
		</div>
	</div>

	<!-- Admin -->
	<div class="container-fluid py-3">
		<div class="row">

			<div class="col-md-12 col-lg-2">
				<div class="list-group">
					<h5 class="list-group-item list-group-item-action">Админ меню</h5>
					<a href="/admin" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Заявки<span class="badge badge-primary badge-pill"><?php echo Master::getCountAvailable(); ?></span></a>
					<a href="/admin/news" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Новости<i class="fas fa-newspaper"></i></a>
					<a href="/admin/prices" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Цены<i class="fas fa-dollar-sign"></i></a>
					<a href="/admin/faq" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Вопросы и ответы<i class="fas fa-question"></i></a>
					<a href="/admin/directions" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Порядок работы<i class="fas fa-gavel"></i></a>
					<a href="/admin/post" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Отделения<i class="fas fa-building"></i></a>
					<a href="/admin/statistics" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center"><i class="fas fa-caret-right"></i>Статистика<i class="fas fa-info-circle"></i></a>
					<a href="/admin/users" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Пользователи<i class="fas fa-users"></i></a>
				</div>
			</div>

			<div class="row col-md-12 col-lg-10">
			
				<div class="col-12 text-muted"><h2>Статистика</h2><hr></div>

				<div class="col-md-12 col-lg-6">
				
						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего завершенных сервисных заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statisticsFull['repair_complete']; ?>"> 
						</div>
						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего отклоненных сервисных заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statisticsFull['repair_cancel']; ?>"> 
						</div>

						<hr>

						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего завершенных выездных заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statisticsFull['field_repair_complete']; ?>">
						</div>
						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего отклоненных выездных заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statisticsFull['field_repair_cancel']; ?>"> 
						</div>

						<hr>

						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего завершенных запрос-заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statisticsFull['calculation_complete']; ?>">
						</div>
						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего отклоненных запрос-заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statisticsFull['calculation_cancel']; ?>"> 
						</div>	

				</div>

				<div class="col-md-12 col-lg-6">
				
						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего завершенных заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statisticsFull['complete']; ?>"> 
						</div>
						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Всего отклоненных заявок</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statisticsFull['cancel']; ?>"> 
						</div>

						<hr>

						<div class="input-group mb-3">
							<div class="input-group-prepend">
							<span class="input-group-text" style="min-width: 320px;">Заявок в очереди</span>
							</div>
							<input type="text" class="form-control" readonly value="<?php echo $statisticsFull['queue']; ?>">
						</div>

				</div>	
			
			</div>

		</div>
	</div>

<?php include ROOT . '/views/layouts/footer.php' ?>