<?php include ROOT . '/views/layouts/header.php' ?>

	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3">
		<div class="container">
			<h1 class="display-4">Админ панель</h1>
			<hr>
			<p class="lead">Внимание, вы вошли в админ панель!!! Будьте крайне внимательны и бдительны!</p>
		</div>
	</div>

	<!-- Admin -->
	<div class="container-fluid py-3">
		<div class="row">

			<div class="col-md-12 col-lg-2">
				<div class="list-group">
					<h5 class="list-group-item list-group-item-action">Админ меню</h5>
					<a href="/admin" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Заявки<span class="badge badge-primary badge-pill"><?php echo Master::getCountAvailable(); ?></span></a>
					<a href="/admin/news" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Новости<i class="fas fa-newspaper"></i></a>
					<a href="/admin/prices" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Цены<i class="fas fa-dollar-sign"></i></a>
					<a href="/admin/faq" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Вопросы и ответы<i class="fas fa-question"></i></a>
					<a href="/admin/directions" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Порядок работы<i class="fas fa-gavel"></i></a>
					<a href="/admin/post" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Отделения<i class="fas fa-building"></i></a>
					<a href="/admin/statistics" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Статистика<i class="fas fa-info-circle"></i></a>
					<a href="/admin/users" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center"><i class="fas fa-caret-right"></i>Пользователи<i class="fas fa-users"></i></a>
				</div>
			</div>

			<div class="col-md-12 col-lg-10">
			
				<div class="col-12 text-muted"><h2>Пользователи</h2><hr></div>

				<div class="col-12">
					<table class="table text-center">
						<thead class="thead-light table-sm">
							<tr>
								<th scope="col"># Идентификатор</th>
								<th scope="col">Пользователь</th>
								<th scope="col">Почта</th>
								<th scope="col">Роль</th>
								<th scope="col">Редактировать</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($usersList as $usersItem): ?>
								<tr>
									<th scope="row"><?php echo $usersItem['id']; ?></th>
									<td><?php echo $usersItem['name']; ?></td>
									<td><?php echo $usersItem['email']; ?></td>
									<td><?php echo $usersItem['role']; ?></td>
									<td>
										<a href="/admin/users/update/<?php echo $usersItem['id']; ?>" class = "btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
					
			
			</div>

		</div>
	</div>

<?php include ROOT . '/views/layouts/footer.php' ?>