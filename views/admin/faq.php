<?php include ROOT . '/views/layouts/header.php' ?>

	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3">
		<div class="container">
			<h1 class="display-4">Админ панель</h1>
			<hr>
			<p class="lead">Внимание, вы вошли в админ панель!!! Будьте крайне внимательны и бдительны!</p>
		</div>
	</div>

	<!-- Admin -->
	<div class="container-fluid py-3">
		<div class="row">

			<div class="col-md-12 col-lg-2">
				<div class="list-group">
					<h5 class="list-group-item list-group-item-action">Админ меню</h5>
					<a href="/admin" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Заявки<span class="badge badge-primary badge-pill"><?php echo Master::getCountAvailable(); ?></span></a>
					<a href="/admin/news" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Новости<i class="fas fa-newspaper"></i></a>
					<a href="/admin/prices" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Цены<i class="fas fa-dollar-sign"></i></a>
					<a href="/admin/faq" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center"><i class="fas fa-caret-right"></i>Вопросы и ответы<i class="fas fa-question"></i></a>
					<a href="/admin/directions" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Порядок работы<i class="fas fa-gavel"></i></a>
					<a href="/admin/post" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Отделения<i class="fas fa-building"></i></a>
					<a href="/admin/statistics" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Статистика<i class="fas fa-info-circle"></i></a>
					<a href="/admin/users" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Пользователи<i class="fas fa-users"></i></a>
				</div>
			</div>

			<div class="row col-md-12 col-lg-10">
			
				<div class="col-12 text-muted"><h2>Вопросы и ответы</h2><hr></div>
				
				<div class="col-12">
					<h3 class="text-muted">Категории</h3>
					
					<a href="/admin/faq/createCategory" class="btn btn-success mb-3"><i class="fas fa-plus"></i> Добавить категорию</a>

					<table class="table text-center">
						<thead class="thead-light table-sm">
							<tr>
								<th scope="col">#</th>
								<th scope="col">Название</th>
								<th scope="col">Порядок сортировки</th>
								<th scope="col">Статус</th>
								<th scope="col">Редактировать</th>
								<th scope="col">Удалить</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1; ?>
							<?php foreach ($faqList as $faqItem): ?>
								<tr>
									<th scope="row"><?php echo $i++; ?></th>
									<td><?php echo $faqItem['name']; ?></td>
									<td><?php echo $faqItem['sort_order']; ?></td>
									<td><?php echo $faqItem['status']; ?></td>
									<td>
										<a href="/admin/faq/updateCategory/<?php echo $faqItem['id']; ?>" class = "btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
									</td>
									<td>
										<button type="button" class = "btn btn-danger btn-sm" data-toggle="modal" data-target="#modalDeleteCategory" id="<?php echo $faqItem['id']; ?>">
											<i class="fas fa-times"></i>
										</button>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>

				<div class="col-md-12 col-lg-12">
					<h3 class="text-muted">Вопросы и ответы</h3>

					<a href="/admin/faq/createFaq" class="btn btn-success mb-3"><i class="fas fa-plus"></i> Добавить вопрос и ответ</a>

					<table class="table text-center">
						<thead class="thead-light table-sm">
							<tr>
								<th scope="col">#</th>
								<th scope="col">Название</th>
								<th scope="col">Ответ</th>
								<th scope="col">Категория</th>
								<th scope="col">Редактировать</th>
								<th scope="col">Удалить</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1; ?>
							<?php foreach ($faqItemsList as $faqItem): ?>
								<tr>
									<th scope="row"><?php echo $i++; ?></th>
									<td><?php echo $faqItem['title']; ?></td>
									<td><?php echo $faqItem['answer']; ?></td>
									<td><?php echo $faqItem['category']; ?></td>
									<td>
										<a href="/admin/faq/updateFaq/<?php echo $faqItem['id']; ?>" class = "btn btn-warning btn-sm"><i class="fas fa-pen"></i></a>
									</td>
									<td>
										<button type="button" class = "btn btn-danger btn-sm" data-toggle="modal" data-target="#modalDeleteFaq" id="<?php echo $faqItem['id']; ?>">
											<i class="fas fa-times"></i>
										</button>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
					
			
			</div>

		</div>
	</div>


	<!-- Modals -->
	<div class="modal fade" id="modalDeleteCategory" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Удаление</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Вы действительно хотите удалить эту категорию? Все привязаные к обучение будут удалены.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
					<a href="" id="deleteCategory" class="btn btn-danger">Удалить</a>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="modalDeleteFaq" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Удаление</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Вы действительно хотите удалить это обучение?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
					<a href="" id="deletePrice" class="btn btn-danger">Удалить</a>
				</div>
			</div>
		</div>
	</div>

<?php include ROOT . '/views/layouts/footer.php' ?>

	<script>
        $('#modalDeleteCategory').on('show.bs.modal', function(e) {
        	$("#deleteCategory").attr('href', '/admin/faq/deleteCategory/' + e.relatedTarget.id);    
        }) 
        $('#modalDeleteFaq').on('show.bs.modal', function(e) {
        	$("#deletePrice").attr('href', '/admin/faq/deleteFaq/' + e.relatedTarget.id);    
        })       
    </script>
