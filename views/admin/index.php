<?php include ROOT . '/views/layouts/header.php' ?>

	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3">
		<div class="container">
			<h1 class="display-4">Админ панель</h1>
			<hr>
			<p class="lead">Внимание, вы вошли в админ панель!!! Будьте крайне внимательны и бдительны!</p>
		</div>
	</div>

	<!-- Admin -->
	<div class="container-fluid py-3">
		<div class="row">

			<div class="col-md-12 col-lg-2">
				<div class="list-group">
					<h5 class="list-group-item list-group-item-action">Админ меню</h5>
					<a href="/admin" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center"><i class="fas fa-caret-right"></i>Заявки<span class="badge badge-primary badge-pill"><?php echo Master::getCountAvailable(); ?></span></a>
					<a href="/admin/news" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Новости<i class="fas fa-newspaper"></i></a>
					<a href="/admin/prices" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Цены<i class="fas fa-dollar-sign"></i></a>
					<a href="/admin/faq" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Вопросы и ответы<i class="fas fa-question"></i></a>
					<a href="/admin/directions" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Порядок работы<i class="fas fa-gavel"></i></a>
					<a href="/admin/post" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Отделения<i class="fas fa-building"></i></a>
					<a href="/admin/statistics" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Статистика<i class="fas fa-info-circle"></i></a>
					<a href="/admin/users" class="list-group-item list-group-item-action bg-light d-flex justify-content-between align-items-center">Пользователи<i class="fas fa-users"></i></a>
				</div>
			</div>

			<div class="col-md-12 col-lg-10">

				<div class="alert alert-info m-0 text-center" role="alert">
					<h4 class="alert-heading">Ожидают</h4>
				</div>

					<table class="table text-center">
					<thead class="thead-light table-sm">
						<tr>
							<th scope="col">#</th>
							<th scope="col">Неисправность</th>
							<th scope="col">Производитель</th>
							<th scope="col">Модель</th>
							<th scope="col">Серийный номер</th>
							<th scope="col">Дата</th>
							<th scope="col">Тип</th>
							<th scope="col">Дополнительно</th>
							<th scope="col">Отклонить</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1; ?>
						<?php foreach ($availableApplicationsList as $availableApplicationsItem): ?>
							<tr>
								<th scope="row"><?php echo $i++; ?></th>
								<td><?php echo $availableApplicationsItem['about']; ?></td>
								<td><?php echo $availableApplicationsItem['producer']; ?></td>
								<td><?php echo $availableApplicationsItem['model']; ?></td>
								<td><?php echo $availableApplicationsItem['serial']; ?></td>
								<td><?php echo $availableApplicationsItem['date']; ?></td>
								<td>
									<?php if($availableApplicationsItem['type'] == 'Сервисная'): ?>
										<span class="badge badge-success">Сервисная</span>
									<?php elseif($availableApplicationsItem['type'] == 'Выездная'): ?>
										<span class="badge badge-warning">Выездная</span>
									<?php else: ?>
										<span class="badge badge-danger">Запрос</span>
									<?php endif; ?>
								</td>
								<td>
									<?php if($availableApplicationsItem['is_urgently']): ?>
										<span class="badge badge-primary">Срочная</span>
									<?php endif; ?>

									<?php if($availableApplicationsItem['is_field']): ?>
										<span class="badge badge-primary">Выезд на место</span>
									<?php endif; ?>

									<?php if($availableApplicationsItem['is_delivery']): ?>
										<span class="badge badge-primary">Доставка</span>
									<?php endif; ?>

									<?php if($availableApplicationsItem['is_mail_answer']): ?>
										<span class="badge badge-primary">Ответ в почту</span>
									<?php endif; ?>
								</td>
								<td>
									<button type="button" class = "btn btn-danger btn-sm" data-toggle="modal" data-target="#modalDelete" id="<?php echo $availableApplicationsItem['id']; ?>">
										<i class="fas fa-times"></i>
									</button>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

				<div class="alert alert-success m-0 text-center" role="alert">
					<h4 class="alert-heading">В работе</h4>
				</div>
					<table class="table text-center">
					<thead class="thead-light table-sm">
						<tr>
							<th scope="col">#</th>
							<th scope="col">Мастер</th>
							<th scope="col">Неисправность</th>
							<th scope="col">Производитель</th>
							<th scope="col">Модель</th>
							<th scope="col">Серийный номер</th>
							<th scope="col">Дата заявки</th>
							<th scope="col">Тип</th> <!-- Сервисная Выездная Запрос и Срочная -->
							<th scope="col">Убрать мастера</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1; ?>
						<?php foreach ($inProgressApplicationsList as $inProgressApplicationsItem): ?>
							<tr>
								<th scope="row"><?php echo $i++; ?></th>
								<td><?php echo $inProgressApplicationsItem['master']; ?></td>
								<td><?php echo $inProgressApplicationsItem['about']; ?></td>
								<td><?php echo $inProgressApplicationsItem['producer']; ?></td>
								<td><?php echo $inProgressApplicationsItem['model']; ?></td>
								<td><?php echo $inProgressApplicationsItem['serial']; ?></td>
								<td><?php echo $inProgressApplicationsItem['date']; ?></td>
								<td>
									<?php if($inProgressApplicationsItem['type'] == 'Сервисная'): ?>
										<span class="badge badge-success">Сервисная</span>
									<?php elseif($inProgressApplicationsItem['type'] == 'Выездная'): ?>
										<span class="badge badge-warning">Выездная</span>
									<?php else: ?>
										<span class="badge badge-danger">Запрос</span>
									<?php endif; ?>
								</td>
								<td>
									<button type="button" class = "btn btn-danger btn-sm" data-toggle="modal" data-target="#modalRemoveMaster" id="<?php echo $inProgressApplicationsItem['id']; ?>">
										<i class="fas fa-times"></i>
									</button>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

				<div class="alert alert-warning m-0 text-center" role="alert">
					<h4 class="alert-heading">Отклоненные</h4>
				</div>
				
					<table class="table text-center">
					<thead class="thead-light table-sm">
						<tr>
							<th scope="col">#</th>
							<th scope="col">Мастер</th>
							<th scope="col">Неисправность</th>
							<th scope="col">Производитель</th>
							<th scope="col">Модель</th>
							<th scope="col">Серийный номер</th>
							<th scope="col">Дата заявки</th>
							<th scope="col">Тип</th> <!-- Сервисная Выездная Запрос и Срочная -->
						</tr>
					</thead>
					<tbody>
						<?php $i = 1; ?>
						<?php foreach ($canceledApplicationsList as $canceledApplicationsItem): ?>
							<tr>
								<th scope="row"><?php echo $i++; ?></th>
								<td><?php echo $canceledApplicationsItem['master']; ?></td>
								<td><?php echo $canceledApplicationsItem['about']; ?></td>
								<td><?php echo $canceledApplicationsItem['producer']; ?></td>
								<td><?php echo $canceledApplicationsItem['model']; ?></td>
								<td><?php echo $canceledApplicationsItem['serial']; ?></td>
								<td><?php echo $canceledApplicationsItem['date']; ?></td>
								<td>
									<?php if($canceledApplicationsItem['type'] == 'Сервисная'): ?>
										<span class="badge badge-success">Сервисная</span>
									<?php elseif($canceledApplicationsItem['type'] == 'Выездная'): ?>
										<span class="badge badge-warning">Выездная</span>
									<?php else: ?>
										<span class="badge badge-danger">Запрос</span>
									<?php endif; ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

				<div class="alert alert-danger m-0 text-center" role="alert">
					<h4 class="alert-heading">Завершенные</h4>
				</div>

					<table class="table text-center">
					<thead class="thead-light table-sm">
						<tr>
							<th scope="col">#</th>
							<th scope="col">Мастер</th>
							<th scope="col">Неисправность</th>
							<th scope="col">Производитель</th>
							<th scope="col">Модель</th>
							<th scope="col">Серийный номер</th>
							<th scope="col">Дата заявки</th>
							<th scope="col">Тип</th> <!-- Сервисная Выездная Запрос и Срочная -->
						</tr>
					</thead>
					<tbody>
						<?php $i = 1; ?>
						<?php foreach ($completedApplicationsList as $completedApplicationsItem): ?>
							<tr>
								<th scope="row"><?php echo $i++; ?></th>
								<td><?php echo $completedApplicationsItem['master']; ?></td>
								<td><?php echo $completedApplicationsItem['about']; ?></td>
								<td><?php echo $completedApplicationsItem['producer']; ?></td>
								<td><?php echo $completedApplicationsItem['model']; ?></td>
								<td><?php echo $completedApplicationsItem['serial']; ?></td>
								<td><?php echo $completedApplicationsItem['date']; ?></td>
								<td>
									<?php if($completedApplicationsItem['type'] == 'Сервисная'): ?>
										<span class="badge badge-success">Сервисная</span>
									<?php elseif($completedApplicationsItem['type'] == 'Выездная'): ?>
										<span class="badge badge-warning">Выездная</span>
									<?php else: ?>
										<span class="badge badge-danger">Запрос</span>
									<?php endif; ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

			</div>

		</div>
	</div>


	<!-- Modals -->
	<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Удаление</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Вы действительно хотите отклонить эту заявку?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
					<a href="" id="cancel" class="btn btn-danger">Отклонить</a>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="modalRemoveMaster" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Убрать мастера</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Вы действительно хотите убрать мастера с этой заявки? Заявка снова будет доступна в общем списке.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
					<a href="" id="removeMaster" class="btn btn-danger">Убрать мастера</a>
				</div>
			</div>
		</div>
	</div>

	<?php if(isset($_SESSION['error'])): ?>
		<div class="modal fade" id="modalErrror" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Ошибка</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p class="my-auto"><?php echo $_SESSION['error']; ?></p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Ок</button>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

<?php include ROOT . '/views/layouts/footer.php' ?>

	<script>
        $('#modalDelete').on('show.bs.modal', function(e) {
        	$("#cancel").attr('href', '/admin/cancel/' + e.relatedTarget.id);    
        })       
        $('#modalRemoveMaster').on('show.bs.modal', function(e) {
        	$("#removeMaster").attr('href', '/admin/removeMaster/' + e.relatedTarget.id);    
        })
    </script>

	<?php if(isset($_SESSION['error'])): ?>
	    <script>
	    	$("#modalErrror").modal()
	    </script>
    <?php endif; unset($_SESSION["error"]); ?>