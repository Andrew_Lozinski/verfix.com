<?php include ROOT . '/views/layouts/header.php' ?>
	
	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3">
		<div class="container">
			<h1 class="display-4">Цены</h1>
			<hr>
			<p class="lead">Здесь вы можете узнать ценовую политику нашего сервиса. Внимание, здесь указаная базовая стоимость услуг, цена может варьироватся в зависимости от сложности работы.</p>
		</div>
	</div>

	<!-- Prices -->
	<div class="container py-4">
		
		<?php foreach ($pricesList as $pricesItem): ?>
			<div>
				<div class="alert alert-success m-0 text-center" role="alert">
					<h4 class="alert-heading"><?php echo $pricesItem['name']; ?></h4>
				</div>
				<table class="table">
					<thead class="thead-light">
						<tr>
							<th scope="col">#</th>
							<th scope="col">Название услуги</th>
							<th scope="col">Цена, грн</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1; ?>
						<?php foreach ($pricesItemsList as $item): ?>
							<?php if($item['prices_name_id'] == $pricesItem['id']): ?>
								<tr>
									<th scope="row"><?php echo $i++; ?></th>
									<td><?php echo $item['title']; ?></td>
									<td>от <?php echo $item['price']; ?> грн.</td>
								</tr>
							<?php endif; ?>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		<?php endforeach; ?>

		<?php foreach ($pricesAdditionList as $pricesAdditionItem): ?>
			<div class="alert alert-warning" role="alert">
				<?php echo $pricesAdditionItem['name']; ?> <strong><?php echo $pricesAdditionItem['price']; ?> грн.</strong>
			</div>
		<?php endforeach; ?>

	</div>

<?php include ROOT . '/views/layouts/footer.php' ?>