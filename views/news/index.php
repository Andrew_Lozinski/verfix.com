<?php include ROOT . '/views/layouts/header.php' ?>
	
	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3">
		<div class="container">
			<h1 class="display-4">Новости</h1>
			<hr>
			<p class="lead">Самые свежие новости нашего сервиса и актуальные новости из мира цифровой техники.</p>
		</div>
	</div>

	<!-- News -->
	<div class="container py-4">

		<div class="row">
			
			<?php foreach ($newsList as $newsItem): ?>
				<div class="col-md-6 col-lg-4 p-3">
					<div class="card">
						<img class="card-img-top" src="<?php echo News::getNewsPic($newsItem['id']); ?>" alt="Card image cap" style="height: 200px;">
						<div class="card-body">
							<h5 class="card-title"><?php echo $newsItem['title']; ?></h5>
							<p class="card-text"><?php echo $newsItem['text']; ?></p>
						</div>
						<div class="card-footer">
							<small class="text-muted"><?php echo $newsItem['date']; ?></small>
						</div>
					</div>
				</div>
			<?php endforeach; ?>

		</div>

	</div>

<?php include ROOT . '/views/layouts/footer.php' ?>