<?php include ROOT . '/views/layouts/header.php' ?>

	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3">
		<div class="container">
			<h1 class="display-4">Заявка на выездной ремонт</h1>
			<hr>
			<p class="lead">Для тех, кто ценит свое время, компания Verfix предлагает оказание услуг на территории клиента.</p>
		</div>
	</div>

	<!-- Field Repair -->
	<div class="container-fluid bg-light">
		<div class="container">

			<?php if(isset($id)): ?>
				<div class="alert alert-success mt-2" role="alert">
					<strong>Готово!</strong> Ваша заявка отправлена!
				</div>
				<div class="alert alert-warning mt-2" role="alert">
					Номер квитанции: <strong><?php echo $client_ticket; ?></strong>
				</div>
				<div class="alert alert-warning mt-2" role="alert">
					Серийный номер: <strong><?php echo $client_serial; ?></strong>
				</div>
				<div class="alert alert-danger mt-2" role="alert">
					<strong>Внимание!</strong> Не потеряйте ваш номер квитанции и серийный номер!
				</div>
				<a class="btn btn-danger mb-2" href="/">Назад на главную страницу</a>
			<?php else: ?>

				<?php if(isset($errors) && is_array($errors)): ?>
					<?php foreach ($errors as $error): ?>
						<div class="alert alert-danger my-2" role="alert">
							<strong>Внимание!</strong> <?php echo $error; ?>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>

				<form class="py-5" method="post" enctype="multipart/form-data">
					<div class="form-row">
						<div class="form-group col-md-6">
							<div class="form-group">
								<input type="text" name="name" class="form-control" placeholder="ФИО, на кого будет выписана официальная квитанция" value="<?php echo $name; ?>" required>
							</div>
							<div class="form-group">
								<input type="email" name="email" class="form-control" placeholder="Почта" value="<?php echo $email; ?>" required>
							</div>
							<div class="form-group">
								<input type="text" name="phone" class="form-control" placeholder="Телефон" value="<?php echo $phone; ?>" required>
							</div>
							<div class="form-group">
								<input type="text" name="street" class="form-control" placeholder="Улица" value="<?php echo $street; ?>" required>
							</div>
							<div class="form-group">
								<input type="text" name="home" class="form-control" placeholder="Номер дома" value="<?php echo $home; ?>" required>
							</div>
							<div class="form-group">
								<input type="text" name="office" class="form-control" placeholder="Офис, квартира" value="<?php echo $office; ?>">
							</div>
							<div class="form-group">
								<input type="text" name="producer" class="form-control" placeholder="Производитель" value="<?php echo $producer; ?>" required>
							</div>
							<div class="form-group">
								<input type="text" name="model" class="form-control" placeholder="Модель устройства" value="<?php echo $model; ?>" required>
							</div>
							<div class="form-group">
								<input type="text" name="serial" class="form-control" placeholder="Серийный номер" value="<?php echo $serial; ?>" required>
							</div>
						</div>
						<div class="form-group col-md-6">
							<div class="form-group">
								<textarea class="form-control" name="about" rows="4" placeholder="Описание неисправности" required><?php echo $about; ?></textarea>
							</div>
							<div class="form-group">
								<label>Прикрепить файл в формате jpg:</label>
								<input type="file" name="image" class="form-control-file">
							</div>
							<div class="form-check form-group">
								<input type="checkbox" class="form-check-input" name="field" checked disabled value="1">
								<label class="form-check-label">Выездной ремонт <span class="badge badge-warning"><?php echo $pricesAdditionList['Выездной ремонт'] ?> грн.</span></label>
							</div>
							<div class="form-check form-group">
								<input type="checkbox" id="inputUrgently" class="form-check-input" name="urgently" value="1">
								<label class="form-check-label" for="inputUrgently">Срочный ремонт <span class="badge badge-warning"><?php echo $pricesAdditionList['Срочный ремонт'] ?> грн.</span></label>
							</div>
						</div>
					</div>
					<div class="form-row align-items-center">
						<div class="form-group col-md-2">
							<button type="submit" name="submit" class="btn btn-primary">Отправить</button>
						</div>
						<div class="form-group col-md-12 alert alert-secondary">
							Стоимость работ может варьироваться от модели устройства и конкретного дефекта. Если Вы по каким-либо причинам откажетесь от проведения предложенного варианта ремонта, Вам придется оплатить стоимость работ на выезде. В тех крайне редких случаях, когда мы не сможем провести ремонт на месте, Ваше устройство будет доставлено на ремонтный участок Verfix и доставлено Вам после ремонта.
						</div>
					</div>
				</form>

			<?php endif; ?>

		</div>
	</div>

<?php include ROOT . '/views/layouts/footer.php' ?>
