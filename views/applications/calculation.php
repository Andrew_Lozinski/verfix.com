<?php include ROOT . '/views/layouts/header.php' ?>

	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3">
		<div class="container">
			<h1 class="display-4">Расчет стоимости ремонта</h1>
			<hr>
			<p class="lead">Благодарим вас за решение воспользоваться функцией предварительного расчета стоимости ремонта!</p>
		</div>
	</div>

	<!-- Calculation -->
	<div class="container-fluid bg-light">
		<div class="container">

			<?php if(isset($id)): ?>
				<div class="alert alert-success mt-2" role="alert">
					<strong>Готово!</strong> Ваша заявка отправлена!
				</div>
				<div class="alert alert-warning mt-2" role="alert">
					Номер квитанции: <strong><?php echo $client_ticket; ?></strong>
				</div>
				<div class="alert alert-warning mt-2" role="alert">
					Серийный номер: <strong><?php echo $client_serial; ?></strong>
				</div>
				<div class="alert alert-danger mt-2" role="alert">
					<strong>Внимание!</strong> Не потеряйте ваш номер квитанции и серийный номер!
				</div>
				<a class="btn btn-danger mb-2" href="/">Назад на главную страницу</a>
			<?php else: ?>

				<?php if(isset($errors) && is_array($errors)): ?>
					<?php foreach ($errors as $error): ?>
						<div class="alert alert-danger my-2" role="alert">
							<strong>Внимание!</strong> <?php echo $error; ?>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>

				<form class="py-5" method="post" enctype="multipart/form-data">
					<div class="form-row">
						<div class="form-group col-md-6">
							<div class="form-group">
								<input type="text" name="name" class="form-control" placeholder="ФИО" value="<?php echo $name; ?>" required>
							</div>
							<div class="form-group">
								<input type="email" name="email" class="form-control" placeholder="Почта" value="<?php echo $email; ?>" required>
							</div>
							<div class="form-check form-group">
								<input type="checkbox" id="inputAnswer" class="form-check-input" name="email_answer" value="1">
								<label class="form-check-label" for="inputAnswer">Ответ в почту. Не перезванивайте мне.</label>
							</div>
							<div class="form-group">
								<input type="text" name="phone" class="form-control" placeholder="Телефон" value="<?php echo $phone; ?>" required>
							</div>
							<div class="form-group">
								<input type="text" name="producer" class="form-control" placeholder="Производитель" value="<?php echo $producer; ?>" required>
							</div>
							<div class="form-group">
								<input type="text" name="model" class="form-control" placeholder="Модель устройства" value="<?php echo $model; ?>" required>
							</div>
							<div class="form-group">
								<input type="text" name="serial" class="form-control" placeholder="Серийный номер" value="<?php echo $serial; ?>" required>
							</div>
						</div>
						<div class="form-group col-md-6">
							<div class="form-group">
								<textarea class="form-control" name="about" rows="4" placeholder="Описание неисправности" required><?php echo $about; ?></textarea>
							</div>
							<div class="form-group">
								<label>Прикрепить файл в формате jpg:</label>
								<input type="file" name="image" class="form-control-file">
							</div>
						</div>
					</div>
					<div class="form-row align-items-center">
						<div class="form-group col-md-2">
							<button type="submit" name="submit" class="btn btn-primary">Отправить</button>
						</div>
						<div class="form-group col-md-12 alert alert-secondary">
							Заполнив форму Вы получите ответ по ориентировочной стоимости запрашиваемого ремонта.
						</div>
					</div>
				</form>

			<?php endif; ?>
		</div>
	</div>

<?php include ROOT . '/views/layouts/footer.php' ?>