<?php include ROOT . '/views/layouts/header.php' ?>
	
	<!-- Jumbotron -->
	<div class="jumbotron jumbotron-fluid m-0 p-3">
		<div class="container">
			<h1 class="display-4">Заявки пользователя</h1>
			<hr>
			<p class="lead">Здесь отображаются все ваши заявки.</p>
		</div>
	</div>

	<!-- Applications -->
	<div class="container-fluid py-4">
		<div class="container">
			<ul class="nav nav-pills mt-4">
				<li class="nav-item">
					<a class="nav-link" href="/profile/repair">Сервисные заявки</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/profile/field_repair">Выездные заявки</a>
				</li>
				<li class="nav-item">
					<a class="nav-link active" href="/profile/calculation">Запросы стоимости</a>
				</li>
			</ul>

			<?php if(!$applicationList): ?>
				<h2 class="text-muted">Нет заявок.</h2>
			<?php endif; ?>

			<?php foreach ($applicationList as $applicationItem): ?>
			<div class="py-3">
				<div class="col-12">
					<h5 class="text-muted">Заявка #<?php echo $applicationItem['id']; ?>
					<small class="text-muted">Производитель: <span class="badge badge-secondary"><?php echo $applicationItem['producer']; ?></span></small>
					<small class="text-muted">Модель: <span class="badge badge-secondary"><?php echo $applicationItem['model']; ?></span></small>
					<small class="text-muted">Дата: <span class="badge badge-secondary"><?php echo $applicationItem['date']; ?></span></small></h5>
				</div>
				<!--<div class="row p-3">
					<div class="col-md-12">
						<div class="progress">
		  					<div class="progress-bar" role="progressbar" style="width: 10%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">10%</div>
						</div>
					</div>
				</div>-->
				<div class="row px-3">
					<div class="col-md-12 col-lg-4">
						<ul class="list-group">
							<?php $is_active = false; ?>

							<?php if($applicationItem['is_active'] == -1): ?>
								<li class="list-group-item text-center bg-danger">Отказано. Этой заявке было отказано в облуживании. Пожалуйста, проверьте корректность данных.</li>
							<?php else: ?>

							<?php if($applicationItem['status'] == 'Информация выдана заказчику') $is_active = true; ?>
							<?php if(!$is_active): ?>
								<li class="list-group-item"><i class="fas fa-times"></i> Информация выдана заказчику</li>
							<?php else: ?>
								<li class="list-group-item list-group-item-success"><i class="fas fa-check"></i> Информация выдана заказчику</li>
							<?php endif; ?>	

							<?php if($applicationItem['status'] == 'Расчет стоимости подведен, отправка уведомления для заказчика') $is_active = true; ?>
							<?php if(!$is_active): ?>
								<li class="list-group-item"><i class="fas fa-times"></i> Расчет стоимости подведен, отправка уведомления для заказчика</li>
							<?php else: ?>
								<li class="list-group-item list-group-item-success"><i class="fas fa-check"></i> Расчет стоимости подведен, отправка уведомления для заказчика</li>
							<?php endif; ?>
							
							<?php if($applicationItem['status'] == 'Просчет стоимости') $is_active = true; ?>
							<?php if(!$is_active): ?>
								<li class="list-group-item"><i class="fas fa-times"></i> Просчет стоимости</li>
							<?php else: ?>
								<li class="list-group-item list-group-item-success"><i class="fas fa-check"></i> Просчет стоимости</li>
							<?php endif; ?>
							
							<?php if($applicationItem['status'] == 'Диагностика неисправности') $is_active = true; ?>
							<?php if(!$is_active): ?>
								<li class="list-group-item"><i class="fas fa-times"></i> Диагностика неисправности</li>
							<?php else: ?>
								<li class="list-group-item list-group-item-success"><i class="fas fa-check"></i> Диагностика неисправности</li>
							<?php endif; ?>

							<?php if($applicationItem['status'] == 'Ожидает') $is_active = true; ?>
							<?php if(!$is_active): ?>
								<li class="list-group-item"><i class="fas fa-times"></i> Ожидает</li>
							<?php else: ?>
								<li class="list-group-item list-group-item-success"><i class="fas fa-check"></i> Ожидает</li>
							<?php endif; ?>

							<?php endif; ?>
						</ul>
					</div>
					<div class="col-md-12 col-lg-8 pt-md-3 pt-lg-0">
						<div class="row text-center">
							<ul class="col-md-12 col-lg-6">
								<li class="list-group-item list-group-item-success">Номер квитанции</li>
								<li class="list-group-item"><?php echo $applicationItem['id']; ?></li>
							</ul>
							
							<ul class="col-md-12 col-lg-6">
								<li class="list-group-item list-group-item-primary">Серийный номер</li>
								<li class="list-group-item"><?php echo $applicationItem['serial']; ?></li>
							</ul>											

							<ul class="col-md-12 col-lg-6">
								<li class="list-group-item list-group-item-secondary">Неисправность</li>
								<li class="list-group-item"><?php echo $applicationItem['about']; ?></li>
							</ul>

							<ul class="col-md-12 col-lg-6">
								<li class="list-group-item list-group-item-secondary">Фото</li>
								<img src="<?php echo Application::getApplicationPic($applicationItem['id']); ?>" width="200" height="200" class="img-thumbnail">
							</ul>

							<ul class="col-md-12 col-lg-6">
								<li class="list-group-item list-group-item-secondary">Стоимость</li>
								<?php if($applicationItem['price'] != 0): ?>
									<li class="list-group-item"><?php echo $applicationItem['price']; ?> грн.</li>
								<?php else: ?>
									<li class="list-group-item">Информация отсутствует</li>
								<?php endif; ?>
							</ul>
						
							<ul class="col-md-12 col-lg-6">
								<li class="list-group-item list-group-item-info">Статус</li>
								<li class="list-group-item"><?php echo $applicationItem['status']; ?></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>


		</div>
	</div>

<?php include ROOT . '/views/layouts/footer.php' ?>