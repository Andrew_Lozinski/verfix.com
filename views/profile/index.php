<?php include ROOT . '/views/layouts/header.php' ?>
	
		<!-- Profile -->
	<div class="container-fluid py-5">
		<div class="container">

			<?php if($result): ?>
				<div class="alert alert-success" role="alert">
					<strong>Готово!</strong> Данные обновлены!
				</div>
			<?php else: ?>
				<?php if(isset($errors) && is_array($errors)): ?>
					<?php foreach ($errors as $error): ?>
						<div class="alert alert-danger" role="alert">
							<strong>Внимание!</strong> <?php echo $error; ?>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			<?php endif; ?>

			<div class="row">
				<div class="col-4">
					<h4>Фото профиля</h4>
					<p>Вы можете изменить свой аватар или удалить текущий аватар.</p>
				</div>
				<div class="col-2">
					<img src="<?php echo User::getProfilePic($user['id']); ?>" width="160" height="160" class="img-thumbnail">
				</div>
				<div class="col-6">
					<form method="post" enctype="multipart/form-data">
						<h5>Загрузить новый аватар</h5>
						<input type="file" name="image" id="ProfileInputPhoto" class="form-control-file">
						<hr>
						<button type="submit" name="new" class="btn btn-outline-success" type="button">Сохранить фото</button>
						<button type="submit" name="delete" class="btn btn-outline-danger" type="button">Убрать фото</button>
					</form>
				</div>
			</div>
			<hr>
			<form method="post">
				<div class="row">
					<div class="col-4">
						<h4>Основные параметры</h4>
						<p>Эта информация появится в вашем профиле.</p>
					</div>
					<div class="col-8">
						<div class="form-group">
							<label for="ProfileInputName">ФИО</label>
							<input type="text" name="name" class="form-control" value="<?php echo $name; ?>" required>
							<small class="form-text text-muted">Введите ваше имя, фамилию, по-отчеству, чтобы мы могли вас узнать</small>
						</div>
						<div class="form-group">
							<label for="ProfileInputEmail">Почта</label>
							<input type="email" name="email" class="form-control" value="<?php echo $email; ?>" required>
							<small class="form-text text-muted">Мы никогда не передадим вашу почту сторонним лицам.</small>
						</div>
						<div class="form-group">
							<label for="ProfileInputPhone">Номер телефона</label>
							<input type="text" name="phone" class="form-control" value="<?php echo $phone; ?>" required>
							<small class="form-text text-muted">Мы никогда не передадим ваш номер телефона сторонним лицам.</small>
						</div>
						<div class="form-group">
							<label>Изменить пароль</label>
							<input type="password" name="password" class="form-control" placeholder="Текущий пароль">
							<div class="dropdown-divider"></div>
							<input type="password" name="password_new" class="form-control" placeholder="Новый пароль">
							<small class="form-text text-muted">Пароль должен состоять не менее из 7 символов.</small>
						</div>
					</div>
				</div>
				<hr>
				<div class="row justify-content-between">
					<a class="btn btn-secondary" href="/profile">Отмена</a>
					<button type="submit" name="submit" class="btn btn-success">Сохранить</button>
				</div>
			</form>
		</div>
	</div>

<?php include ROOT . '/views/layouts/footer.php' ?>