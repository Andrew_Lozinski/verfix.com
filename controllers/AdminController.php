<?php

class AdminController extends AdminBase
{

	public function actionIndex()
	{
		// Проверяем доступ
		self::checkAdmin();

		$availableApplicationsList = array();
		$availableApplicationsList = Master::getAvailableApplicationsList();

		$inProgressApplicationsList = array();
		$inProgressApplicationsList = Admin::getInProgressApplicationsList();

		$canceledApplicationsList = array();
		$canceledApplicationsList = Admin::getCanceledApplicationsList();

		$completedApplicationsList = array();
		$completedApplicationsList = Admin::getCompletedApplicationsList();

		require_once(ROOT . '/views/admin/index.php');

		return true;
	}

	public function actionNews()
	{
		// Проверяем доступ
		self::checkAdmin();

		$newsList = array();
		$newsList = News::getNewsList();

		require_once(ROOT . '/views/admin/news.php');

		return true;
	}

	public function actionPrices()
	{
		// Проверяем доступ
		self::checkAdmin();

		$pricesList = array();
		$pricesList = Admin::getPricesList();

		$pricesItemsList = array();
		$pricesItemsList = Admin::getPricesItemsList();

		$pricesAdditionList = array();
		$pricesAdditionList = Prices::getAdditionList();

		require_once(ROOT . '/views/admin/prices.php');

		return true;
	}

	public function actionFaq()
	{
		// Проверяем доступ
		self::checkAdmin();

		$faqList = array();
		$faqList = Admin::getFaqList();

		$faqItemsList = array();
		$faqItemsList = Admin::getFaqItemsList();

		require_once(ROOT . '/views/admin/faq.php');

		return true;
	}

	public function actionDirections()
	{
		// Проверяем доступ
		self::checkAdmin();

		$directionsList = array();
		$directionsList = Directions::getDirections();

		require_once(ROOT . '/views/admin/directions.php');

		return true;
	}

	public function actionPost()
	{
		// Проверяем доступ
		self::checkAdmin();

		$postList = array();
		$postList = Post::getPostList();

		require_once(ROOT . '/views/admin/post.php');

		return true;
	}

	public function actionStatistics()
	{
		// Проверяем доступ
		self::checkAdmin();

		$statisticsFull = Admin::getAllStatistics();

		require_once(ROOT . '/views/admin/statistics.php');

		return true;
	}

	public function actionUsers()
	{
		// Проверяем доступ
		self::checkAdmin();

		$usersList  = array();
		$usersList = Admin::getUsersList();

		require_once(ROOT . '/views/admin/users.php');

		return true;
	}

}