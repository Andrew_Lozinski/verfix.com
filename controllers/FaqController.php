<?php

class FaqController
{

	public function actionIndex()
	{
		$faqList = array();
		$faqList = Faq::getFaqList();

		$faqItemsList = array();
		$faqItemsList = Faq::getFaqItemsList();

		require_once(ROOT . '/views/faq/index.php');

		return true;
	}

}