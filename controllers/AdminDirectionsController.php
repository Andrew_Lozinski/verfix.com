<?php

class AdminDirectionsController extends AdminBase
{
	public function actionCreate()
    {
        // Проверка доступа
        self::checkAdmin();

        $title = '';
        $text = '';

        $errors = false;

		if(isset($_POST['submit'])) {

			$title = $_POST['title'];
			$text = $_POST['text'];

			// Валидация полей
	        if (!Admin::checkFill($title, 5)) {
	            $errors[] = 'Заговолок не должен быть короче 5 символов.';
	        }
	        if (!Admin::checkFill($text, 10)) {
	            $errors[] = 'Описание не должно быть короче 10 символов.';
	        }

	        if ($errors == false) {
                // Если ошибок нет

                Directions::addDirections($title, $text);


                header("Location: /admin/directions");
                
            }


		}


        require_once(ROOT . '/views/admin_directions/create.php');
        return true;
    }

    public function actionUpdate($id)
    {
        // Проверка доступа
        self::checkAdmin();

        $directionItem = Directions::getDirectionItemById($id);

        $title = $directionItem['title'];
        $text = $directionItem['text'];

        $errors = false;

		if(isset($_POST['submit'])) {

			$title = $_POST['title'];
			$text = $_POST['text'];

			// Валидация полей
	        if (!Admin::checkFill($title, 5)) {
	            $errors[] = 'Заговолок не должен быть короче 5 символов.';
	        }
	        if (!Admin::checkFill($text, 10)) {
	            $errors[] = 'Описание не должно быть короче 10 символов.';
	        }

	        if ($errors == false) {
                // Если ошибок нет

                Directions::updateDirections($id, $title, $text);

                header("Location: /admin/directions");
            }

		}

		require_once(ROOT . '/views/admin_directions/update.php');
        return true;
    }

    public function actionDelete($id)
    {
        // Проверка доступа
        self::checkAdmin();

        Directions::deleteDirections($id);

        header("Location: /admin/directions");

        return true;
    }
}