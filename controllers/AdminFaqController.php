<?php

class AdminFaqController extends AdminBase
{
	public function actionCreateCategory()
	{
		// Проверка доступа
        self::checkAdmin();

        $name = '';
        $sort_order = '';

        $errors = false;

		if(isset($_POST['submit'])) {

			$name = $_POST['name'];
			$sort_order = $_POST['sort_order'];
			$status = $_POST['status'];

			// Валидация полей
	        if (!Admin::checkFill($name, 5)) {
	            $errors[] = 'Название не должно быть короче 5 символов.';
	        }
	        if(filter_var($sort_order, FILTER_VALIDATE_INT) === false)
				$errors[] = 'Порядок сортировки должен иметь целочисленое значение.';

	        if ($errors == false) {
                // Если ошибок нет

                Faq::addCategory($name, $sort_order, $status);

                header("Location: /admin/faq");
            }


		}


        require_once(ROOT . '/views/admin_faq/createCategory.php');
        return true;
	}

	public function actionUpdateCategory($id)
	{
		// Проверка доступа
        self::checkAdmin();

        $categoryItem = Faq::getCategoryItemById($id);

        $name = $categoryItem['name'];
        $sort_order = $categoryItem['sort_order'];
        $status = $categoryItem['status'];

        $errors = false;

		if(isset($_POST['submit'])) {

			$name = $_POST['name'];
			$sort_order = $_POST['sort_order'];
			$status = $_POST['status'];

			// Валидация полей
	        if (!Admin::checkFill($name, 5)) {
	            $errors[] = 'Название не должно быть короче 5 символов.';
	        }
	        if(filter_var($sort_order, FILTER_VALIDATE_INT) === false)
				$errors[] = 'Порядок сортировки должен иметь целочисленое значение.';

	        if ($errors == false) {
                // Если ошибок нет

                Faq::updateCategory($id, $name, $sort_order, $status);

                header("Location: /admin/faq");
            }


		}


        require_once(ROOT . '/views/admin_faq/updateCategory.php');
        return true;
	}

	public function actionDeleteCategory($id)
    {
        // Проверка доступа
        self::checkAdmin();

        Faq::deleteCategory($id);

        header("Location: /admin/faq");

        return true;
    }

	public function actionCreateFaq()
	{
		// Проверка доступа
        self::checkAdmin();

        $faqList = array();
		$faqList = Admin::getFaqList();

        $title = '';
        $answer = '';
        $category = 0;

        $errors = false;

		if(isset($_POST['submit'])) {

			$title = $_POST['title'];
			$answer = $_POST['answer'];
			$category = $_POST['category'];

			// Валидация полей
	        if (!Admin::checkFill($title, 10)) {
	            $errors[] = 'Вопрос не должен быть короче 10 символов.';
	        }
	        if (!Admin::checkFill($answer, 10)) {
	            $errors[] = 'Ответ не должен быть короче 10 символов.';
	        }

	        if ($errors == false) {
                // Если ошибок нет

                Faq::addFaq($title, $answer, $category);

                header("Location: /admin/faq");
            }


		}


        require_once(ROOT . '/views/admin_faq/createFaq.php');
        return true;
	}


	public function actionUpdateFaq($id)
	{
		// Проверка доступа
        self::checkAdmin();

        $faqItem = Faq::getFaqItemById($id);

        $faqList = array();
		$faqList = Admin::getFaqList();

        $title = $faqItem['title'];
        $answer = $faqItem['answer'];
        $category = $faqItem['faq_name_id'];

        $errors = false;

		if(isset($_POST['submit'])) {

			$title = $_POST['title'];
			$answer = $_POST['answer'];
			$category = $_POST['category'];

			// Валидация полей
	        if (!Admin::checkFill($title, 10)) {
	            $errors[] = 'Вопрос не должен быть короче 10 символов.';
	        }
	        if (!Admin::checkFill($answer, 10)) {
	            $errors[] = 'Ответ не должен быть короче 10 символов.';
	        }

	        if ($errors == false) {
                // Если ошибок нет

                Faq::updateFaq($id, $title, $answer, $category);

                header("Location: /admin/faq");
            }


		}


        require_once(ROOT . '/views/admin_faq/updateFaq.php');
        return true;
	}

	public function actionDeleteFaq($id)
    {
        // Проверка доступа
        self::checkAdmin();

        Faq::deleteFaq($id);

        header("Location: /admin/faq");

        return true;
    }

}