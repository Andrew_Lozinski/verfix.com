<?php

class AdminApplicationController extends AdminBase
{
	public function actionCancel($id)
	{
		// Проверяем доступ
		self::checkAdmin();
		
		$userId = User::checkLogged();
		if(Master::isMasterAvailable($userId)){
			Master::cancel($id, $userId);

			header('Location: /admin');
		}
		else{
			// Записываем ошибку в сессию
        	$_SESSION['error'] = 'Вы уже выбрали заявку. Сначало закончите её.';
			header('Location: /admin');
		}

		return true;
	}


	public function actionRemoveMaster($applicationId)
	{
		// Проверяем доступ
		self::checkAdmin();

		Admin::removeMaster($applicationId);

		header('Location: /admin');

		return true;
	}


}