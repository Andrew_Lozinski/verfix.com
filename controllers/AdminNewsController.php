<?php

class AdminNewsController extends AdminBase
{
	public function actionCreate()
    {
        // Проверка доступа
        self::checkAdmin();

        $title = '';
        $text = '';

        $errors = false;

		if(isset($_POST['submit'])) {

			$title = $_POST['title'];
			$text = $_POST['text'];

			// Валидация полей
	        if (!Admin::checkFill($title, 5)) {
	            $errors[] = 'Заговолок не должен быть короче 5 символов.';
	        }
	        if (!Admin::checkFill($text, 10)) {
	            $errors[] = 'Описание не должно быть короче 10 символов.';
	        }

	        if ($errors == false) {
                // Если ошибок нет

                $id = News::add($title, $text);

                 // Если запись добавлена
                if ($id) {
                    // Проверим, загружалось ли через форму изображение
                    if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                        // Если загружалось, переместим его в нужную папке, дадим новое имя
                        move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/data/news/{$id}.jpg");
                    }

                    header("Location: /admin/news");
                };
            }


		}


        require_once(ROOT . '/views/admin_news/create.php');
        return true;
    }

    public function actionUpdate($id)
    {
        // Проверка доступа
        self::checkAdmin();

        $newsItem = News::getNewsItemById($id);

        $title = $newsItem['title'];
        $text = $newsItem['text'];

        $errors = false;

		if(isset($_POST['submit'])) {

			$title = $_POST['title'];
			$text = $_POST['text'];

			// Валидация полей
	        if (!Admin::checkFill($title, 5)) {
	            $errors[] = 'Заговолок не должен быть короче 5 символов.';
	        }
	        if (!Admin::checkFill($text, 10)) {
	            $errors[] = 'Описание не должно быть короче 10 символов.';
	        }

	        if ($errors == false) {
                // Если ошибок нет

                News::update($id, $title, $text);

                if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                    // Если загружалось, переместим его в нужную папке, дадим новое имя
                    move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/data/news/{$id}.jpg");
                }

                header("Location: /admin/news");
            }


		}

		require_once(ROOT . '/views/admin_news/update.php');
        return true;
    }

    public function actionDelete($id)
    {
        // Проверка доступа
        self::checkAdmin();

        News::delete($id);

        header("Location: /admin/news");

        return true;
    }
}