<?php

class ProfileController
{

	public function actionIndex()
	{
		clearstatcache();
		
		$userId = User::checkLogged();

		$user = User::getUserByID($userId);

		$name = $user['name'];
		$email = $user['email'];
		$phone = $user['phone'];

		$result = false;

		$errors = false;

		if(isset($_POST['new'])) {
            if (is_uploaded_file($_FILES["image"]["tmp_name"]))
                move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/data/profiles/{$userId}.jpg");
		}

		if(isset($_POST['delete'])) {
			// Путь к папке с фотографиями
	        $path = '/data/profiles/';

	        // Путь к изображению профиля
	        $pathToProductImage = $path . $userId . '.jpg';

			if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToProductImage)){
				unlink($_SERVER['DOCUMENT_ROOT'].$pathToProductImage);
			}
		}

		if(isset($_POST['submit'])) {
			$name = $_POST['name'];
			$email = $_POST['email'];
			$phone = $_POST['phone'];
			$password = $_POST['password'];
			$password_new = $_POST['password_new'];

			if (!User::checkName($name)) {
	            $errors[] = 'ФИО не должно быть короче 10 символов.';
	        }
	        if (!User::checkEmail($email)) {
	            $errors[] = 'Неккоректный email.';
	        }
	        if (!User::checkPhone($phone)) {
	            $errors[] = 'Некорректный телефон.';
	        }

		    if($password != '' || $password_new != '') {
				if (!User::checkPasswordPair($password, $user['password'])) {
					$errors[] = 'Неверный пароль.';
				}
		       	if (!User::checkPassword($password_new)) {
	                $errors[] = 'Новый пароль не должен быть короче 7-ти символов.';
	            }
	            $password = $password_new;
	        }
	        else {
	        	$password = $user['password'];
	        }
           
           	if($email != $user['email']) {
           		if (User::checkEmailExists($email)) {
	                $errors[] = 'Такой email уже используется.';
	            }
           	}

            if ($errors == false){
            	$result = User::edit($userId, $name, $email, $phone, $password);
            }
		}


		require_once(ROOT . '/views/profile/index.php');

		return true;
	}

	public function actionRepairView()
	{
		$userId = User::checkLogged();

		//$user = User::getUserByID($userId);

		$applicationList = array();
		$applicationList = Profile::getRepairApplicationsByUserId($userId);
		
		require_once(ROOT . '/views/profile/applicationRepair.php');
		return true;
	}

	public function actionFieldView()
	{
		$userId = User::checkLogged();

		$user = User::getUserByID($userId);

		$applicationList = array();
		$applicationList = Profile::getFieldApplicationsByUserId($userId);

		require_once(ROOT . '/views/profile/applicationField.php');
		return true;
	}

	public function actionCalculationView()
	{
		$userId = User::checkLogged();

		$user = User::getUserByID($userId);

		$applicationList = array();
		$applicationList = Profile::getCalculationApplicationsByUserId($userId);

		require_once(ROOT . '/views/profile/applicationCalculation.php');
		return true;
	}

}