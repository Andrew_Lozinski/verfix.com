<?php

class PricesController
{

	public function actionIndex()
	{
		$pricesList = array();
		$pricesList = Prices::getPricesList();

		$pricesItemsList = array();
		$pricesItemsList = Prices::getPricesItemsList();

		$pricesAdditionList = array();
		$pricesAdditionList = Prices::getAdditionList();

		require_once(ROOT . '/views/prices/index.php');

		return true;
	}

}