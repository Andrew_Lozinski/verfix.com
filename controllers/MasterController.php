<?php

class MasterController extends MasterBase
{

	public function actionIndex()
	{
		// Проверяем доступ
		self::checkMaster();

		$availableApplicationsList = array();
		$availableApplicationsList = Master::getAvailableApplicationsList();

		require_once(ROOT . '/views/master/index.php');

		return true;
	}

	public function actionCurrent()
	{
		// Проверяем доступ
		self::checkMaster();

		$userId = User::checkLogged();
		$applicationItem = Master::getCurrentApplication($userId);

		$status = $applicationItem['status'];
		$price = $applicationItem['price'];
		$report = $applicationItem['report'];

		$type = Master::getCurrentApplicationType($userId);

		$errors = false;
		$result = false;

		if(isset($_POST['submit'])) {
			$status = $_POST['status'];
			$price = $_POST['price'];
			$report = $_POST['report'];

			if(filter_var($price, FILTER_VALIDATE_INT) === false)
				$errors[] = 'Цена должна иметь целочисленое значение.';

			if($errors == false)
				$result = Master::update($status, $price, $report, $applicationItem['id'], $type);
		}

		require_once(ROOT . '/views/master/current.php');

		return true;
	}

	public function actionDirections()
	{
		// Проверяем доступ
		self::checkMaster();

		$directionsList = array();
		$directionsList = Directions::getDirections();

		require_once(ROOT . '/views/master/directions.php');

		return true;
	}

	public function actionStatistics()
	{
		// Проверяем доступ
		self::checkMaster();

		$userId = User::checkLogged();

		$date = date('Y-m-d');
		$interval = "за сегодня";

		$statistics = Master::getStatisticsByDate($userId, $date);
		$statisticsFull = Master::getAllStatistics($userId);


		if(isset($_POST['submit']) && isset($_POST['date'])) {
			$date = $_POST['date'];

			if($date == 1){
				$date = date('Y-m-d');
				$interval = "за сегодня";
			}
			else if($date == 2){
				$date = date('Y-m-d', strtotime('first day of this month'));
				$interval = "за месяц";
			}
			else if($date == 3){
				$date = date('Y-m-d', strtotime('first day of -2 month'));
				$interval = "за квартал";
			}
			else{
				$date = date('Y-m-d', strtotime('first day of january this year'));
				$interval = "за год";
			}

			$statistics = Master::getStatisticsByDate($userId, $date);
		}

		require_once(ROOT . '/views/master/statistics.php');

		return true;
	}

	public function actionArchive()
	{
		// Проверяем доступ
		self::checkMaster();

		$userId = User::checkLogged();

		$applicationsList = array();
		$applicationsList = Master::getArchive($userId);


		if(isset($_POST['submit'])) {
			$template = $_POST['template'];

			$applicationsList = array();
			$applicationsList = Master::getArchive($userId, $template);
			require_once(ROOT . '/views/master/archive.php');
		}

		require_once(ROOT . '/views/master/archive.php');

		return true;
	}
}