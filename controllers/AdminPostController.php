<?php

class AdminPostController extends AdminBase
{
	public function actionCreate()
    {
        // Проверка доступа
        self::checkAdmin();

        $name = '';

        $errors = false;

		if(isset($_POST['submit'])) {

			$name = $_POST['name'];

			// Валидация полей
	        if (!Admin::checkFill($name, 5)) {
	            $errors[] = 'Название не должно быть короче 5 символов.';
	        }

	        if ($errors == false) {
                // Если ошибок нет

                Post::addPost($name);


                header("Location: /admin/post");
                
            }


		}


        require_once(ROOT . '/views/admin_post/create.php');
        return true;
    }

    public function actionUpdate($id)
    {
        // Проверка доступа
        self::checkAdmin();

        $postItem = Post::getPostItemById($id);

        $name = $postItem['name'];

        $errors = false;

		if(isset($_POST['submit'])) {

			$name = $_POST['name'];

			// Валидация полей
	        if (!Admin::checkFill($name, 5)) {
	            $errors[] = 'Название не должно быть короче 5 символов.';
	        }

	        if ($errors == false) {
                // Если ошибок нет

                Post::updatePost($id, $name);

                header("Location: /admin/post");
            }

		}

		require_once(ROOT . '/views/admin_post/update.php');
        return true;
    }

    public function actionDelete($id)
    {
        // Проверка доступа
        self::checkAdmin();

        Post::deletePost($id);

        header("Location: /admin/post");

        return true;
    }
}