<?php

class MasterApplicationController extends MasterBase
{
	public function actionConfirm($id)
	{
		// Проверяем доступ
		self::checkMaster();
		
		$userId = User::checkLogged();
		if(Master::isMasterAvailable($userId)){
			Master::add($id, $userId);

			header('Location: /master/current');
		}
		else {
			// Записываем ошибку в сессию
        	$_SESSION['error'] = 'Вы уже выбрали заявку. Сначало закончите её.';
			header('Location: /master');
		}

		return true;
	}

	public function actionCancel($id)
	{
		// Проверяем доступ
		self::checkMaster();
		
		$userId = User::checkLogged();
		if(Master::isMasterAvailable($userId)){
			Master::cancel($id, $userId);

			header('Location: /master');
		}
		else{
			// Записываем ошибку в сессию
        	$_SESSION['error'] = 'Вы уже выбрали заявку. Сначало закончите её.';
			header('Location: /master');
		}

		return true;
	}


	public function actionCurrentCancel($id)
	{
		// Проверяем доступ
		self::checkMaster();

		$userId = User::checkLogged();

		Master::cancel($id, $userId);

		header('Location: /master/current');

		return true;
	}
	
	public function actionIgnore($applicationId)
	{
		// Проверяем доступ
		self::checkMaster();

		$userId = User::checkLogged();

		Master::ignore($userId, $applicationId);

		header('Location: /master/current');

		return true;
	}
	
	public function actionComplete($applicationId)
	{
		// Проверяем доступ
		self::checkMaster();

		$userId = User::checkLogged();

		Master::complete($userId, $applicationId);

		header('Location: /master/current');
		
		return true;
	}
}