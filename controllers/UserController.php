<?php

class UserController
{

	public function actionRegister()
	{
        if(!User::isGuest())
            header("Location: /profile");

		$name = '';
		$email = '';
		$phone = '';

		if(isset($_POST['submit'])) {
			$name = $_POST['name'];
			$email = $_POST['email'];
			$phone = $_POST['phone'];
			$password_one = $_POST['password_one'];
			$password_two = $_POST['password_two'];

			$errors = false;

			// Валидация полей
            if (!User::checkName($name)) {
                $errors[] = 'ФИО не должно быть короче 10 символов.';
            }
            if (!User::checkEmail($email)) {
                $errors[] = 'Неккоректный email.';
            }
            if (!User::checkPhone($phone)) {
                $errors[] = 'Некорректный телефон.';
            }
            if (!User::checkPasswordPair($password_one, $password_two)) {
                $errors[] = 'Пароли не совпадают. Пароль не должен быть короче 7-ми символов.';
            }
            if (User::checkEmailExists($email)) {
                $errors[] = 'Такой email уже используется.';
            }

           	if ($errors == false) {
                // Если ошибок нет
                // Регистрируем пользователя
                $id = User::register($name, $email, $password_one, $phone);

                 // Если запись добавлена
                if ($id) {
                    // Проверим, загружалось ли через форму изображение
                    if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                        // Если загружалось, переместим его в нужную папке, дадим новое имя
                        move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/data/profiles/{$id}.jpg");
                    }
                };
                User::auth($id);

                header("Location: /profile");          
            }
		}

		require_once(ROOT . '/views/user/register.php');

		return true;
	}


    public function actionLogin()
    {
        if(!User::isGuest())
            header("Location: /profile");

        $email = '';

        if(isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = false;

            // Валидация полей
            if (!User::checkEmail($email)) {
                $errors[] = 'Неправильный email.';
            }
            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль не должен быть короче 7-ми символов.';
            }

            $userId = User::checkUserData($email, $password);

            if ($userId == false) {
                // Если данные неправильные - показываем ошибку
                $errors[] = 'Неправильные данные для входа на сайт.';
            } else {
                // Если данные правильные, запоминаем пользователя (сессия)
                User::auth($userId);

                // Перенаправляем пользователя в закрытую часть - кабинет 
                header("Location: /profile");
            }
        }

        require_once(ROOT . '/views/user/login.php');

        return true;
    }

    public function actionLogout()
    {        
        // Удаляем информацию о пользователе из сессии
        unset($_SESSION["user"]);
        
        // Перенаправляем пользователя на главную страницу
        header("Location: /");
    }

}