<?php

class AdminUsersController extends AdminBase
{
	public function actionUpdate($id)
    {
        // Проверка доступа
        self::checkAdmin();

        $user = User::getUserById($id);

        $name = $user['name'];
        $email = $user['email'];
        $role = $user['role'];

        $errors = false;

		if(isset($_POST['submit'])) {

			$role = $_POST['role'];

            Admin::updateUser($id, $role);

            header("Location: /admin/users");
		}

		require_once(ROOT . '/views/admin_users/update.php');
        return true;
    }
}