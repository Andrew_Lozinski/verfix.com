<?php

class ApplicationController
{
	public function actionShowAjax()
	{
		$id = $_POST['ajax_id'];
		$serial = $_POST['ajax_serial'];

		if(filter_var($id, FILTER_VALIDATE_INT) !== false && $serial != ''){
			$application = Application::getApplicationAjax($id, $serial);

			if(isset($application['status']) && isset($application['price']))
				echo '
					<div class="alert alert-secondary" role="alert">
						Текущий статус: <strong>'.$application['status'].'</strong>
					</div>
					<div class="alert alert-secondary" role="alert">
						Стоимость: <strong>'.$application['price'].' грн.</strong>
					</div>
				';
			else{
				echo '
					<div class="alert alert-secondary" role="alert">
						Заявка <strong>не найдена!</strong>
					</div>
					';
			}
		}
		else {
			echo '
				<div class="alert alert-secondary" role="alert">
					Заявка <strong>не найдена!</strong>
				</div>
				';
		}

		return true;
	}

	public function actionRepair()
	{
		$name = '';
		$email = '';
		$phone = '';
		$producer = '';
		$model = '';
		$serial = '';
		$about = '';
		$additionally = '';
		$street = '';
		$home = '';
		$office = '';

		$pricesAdditionList = array();
		$pricesAdditionList = Application::getAdditionPriceList();

		$postList = array();
		$postList = Post::getPostList();

		if(!User::isGuest()){
			$userId = User::checkLogged();
			$user = User::getUserByID($userId);

			$name = $user['name'];
			$email = $user['email'];
			$phone = $user['phone'];
		}

		if(isset($_POST['submit'])) {

			$errors = false;

			$name = $_POST['name'];
			$email = $_POST['email'];
			$phone = $_POST['phone'];
			$producer = $_POST['producer'];
			$model = $_POST['model'];
			$serial = $_POST['serial'];
			$about = $_POST['about'];
			$additionally = $_POST['additionally'];

			// Валидация полей
            if (!User::checkName($name)) {
                $errors[] = 'ФИО не должно быть короче 10 символов.';
            }
            if (!User::checkEmail($email)) {
                $errors[] = 'Неккоректный email.';
            }
            if (!User::checkPhone($phone)) {
                $errors[] = 'Некорректный телефон.';
            }
            if(!Application::checkFill($producer)) {
            	$errors[] = 'Некорректный производитель.';
            }
            if(!Application::checkFill($model)) {
            	$errors[] = 'Некорректная модель устройства.';
            }        
            if(!Application::checkFill($serial)) {
            	$errors[] = 'Некорректный серийный номер.';
            }
            if(!Application::checkFill($about)) {
            	$errors[] = 'Неккоректное описание неисправности.';
            }

			if(isset($_POST['urgently']))
				$is_urgently = $_POST['urgently'];
			else
				$is_urgently = 0;

			if(isset($_POST['delivery'])) {
				$is_delivery = $_POST['delivery'];

				if(isset($_POST['post']))
					$post_service = $_POST['post'];
				else $post_service = '';

	            if(!Application::checkFill($post_service)) {
	            	$errors[] = 'Выберите почтовый сервис.';
	            }

				$street = $_POST['street'];
				$home = $_POST['home'];
				$office = $_POST['office'];

	            if(!Application::checkFill($street)) {
	            	$errors[] = 'Некорректная улица.';
	            }
	            	            	         
			} else {
				$is_delivery = 0;
				$post_service = '';
				$street = '';
				$home = '';
				$office = '';
			}

			if(!User::isGuest())
				$userId = User::checkLogged();
			else $userId = 0;

			if ($errors == false) {
                // Если ошибок нет
                // Регистрируем заявку
                $id = Application::addRepair($name, $email, $phone, $producer, $model, $serial, $is_urgently, $is_delivery, $post_service, $street, $home, $office, $about, $additionally, $userId);

                 // Если запись добавлена
                if ($id) {
                    // Проверим, загружалось ли через форму изображение
                    if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                        // Если загружалось, переместим его в нужную папке, дадим новое имя
                        move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/data/applications/{$id}.jpg");
                    }
                };

        		$client_ticket = $id;
        		$client_serial = $serial;
            }

		}

		require_once(ROOT . '/views/applications/repair.php');
		return true;
	}

	public function actionField()
	{
		$name = '';
		$email = '';
		$phone = '';
		$street = '';
		$home = '';
		$office = '';
		$producer = '';
		$model = '';
		$serial = '';
		$about = '';

		$pricesAdditionList = array();
		$pricesAdditionList = Application::getAdditionPriceList();

		if(!User::isGuest()){
			$userId = User::checkLogged();
			$user = User::getUserByID($userId);

			$name = $user['name'];
			$email = $user['email'];
			$phone = $user['phone'];
		}

		if(isset($_POST['submit'])) {

			$errors = false;

			$name = $_POST['name'];
			$email = $_POST['email'];
			$phone = $_POST['phone'];
			$street = $_POST['street'];
			$home = $_POST['home'];
			$office = $_POST['office'];
			$producer = $_POST['producer'];
			$model = $_POST['model'];
			$serial = $_POST['serial'];
			$about = $_POST['about'];

			if(isset($_POST['urgently']))
				$is_urgently = $_POST['urgently'];
		 	else
		 		$is_urgently = 0;

		 	$is_field = 1;

		 	// Валидация полей
            if (!User::checkName($name)) {
                $errors[] = 'ФИО не должно быть короче 10 символов.';
            }
            if (!User::checkEmail($email)) {
                $errors[] = 'Неккоректный email.';
            }
            if (!User::checkPhone($phone)) {
                $errors[] = 'Некорректный телефон.';
            }
            if(!Application::checkFill($street)) {
            	$errors[] = 'Неккоректная улица.';
            }
            if(!Application::checkFill($producer)) {
            	$errors[] = 'Некорректный производитель.';
            }
            if(!Application::checkFill($model)) {
            	$errors[] = 'Некорректная модель устройства.';
            }        
            if(!Application::checkFill($serial)) {
            	$errors[] = 'Некорректный серийный номер.';
            }
            if(!Application::checkFill($about)) {
            	$errors[] = 'Неккоректное описание неисправности.';
            }

            if(!User::isGuest())
				$userId = User::checkLogged();
			else $userId = 0;

			if ($errors == false) {
                // Если ошибок нет
                // Регистрируем заявку
                $id = Application::addField($name, $email, $phone, $street, $home, $office, $producer, $model, $serial, $about, $is_field, $is_urgently, $userId);

                 // Если запись добавлена
                if ($id) {
                    // Проверим, загружалось ли через форму изображение
                    if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                        // Если загружалось, переместим его в нужную папке, дадим новое имя
                        move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/data/applications/{$id}.jpg");
                    }
                };

                $client_ticket = $id;
        		$client_serial = $serial;
            }
		}

		require_once(ROOT . '/views/applications/field_repair.php');
		return true;
	}

	public function actionCalculation()
	{
		$name = '';
		$email = '';
		$phone = '';
		$producer = '';
		$model = '';
		$serial = '';
		$about = '';

		if(!User::isGuest()){
			$userId = User::checkLogged();
			$user = User::getUserByID($userId);

			$name = $user['name'];
			$email = $user['email'];
			$phone = $user['phone'];
		}

		if(isset($_POST['submit'])) {

			$errors = false;

			$name = $_POST['name'];
			$email = $_POST['email'];
			$phone = $_POST['phone'];
			$producer = $_POST['producer'];
			$model = $_POST['model'];
			$serial = $_POST['serial'];
			$about = $_POST['about'];

			if(isset($_POST['email_answer']))
				$email_answer = $_POST['email_answer'];
		 	else
		 		$email_answer = 0;

		 	// Валидация полей
            if (!User::checkName($name)) {
                $errors[] = 'ФИО не должно быть короче 10 символов.';
            }
            if (!User::checkEmail($email)) {
                $errors[] = 'Неккоректный email.';
            }
            if (!User::checkPhone($phone)) {
                $errors[] = 'Некорректный телефон.';
            }
            if(!Application::checkFill($producer)) {
            	$errors[] = 'Некорректный производитель.';
            }
            if(!Application::checkFill($model)) {
            	$errors[] = 'Некорректная модель устройства.';
            }
            if(!Application::checkFill($serial)) {
            	$errors[] = 'Некорректный серийный номер.';
            }
            if(!Application::checkFill($about)) {
            	$errors[] = 'Неккоректное описание неисправности.';
            }

            if(!User::isGuest())
				$userId = User::checkLogged();
			else $userId = 0;

			if ($errors == false) {
                // Если ошибок нет
                // Регистрируем заявку
                $id = Application::addCalculation($name, $email, $phone, $producer, $model, $serial, $about, $email_answer, $userId);

                 // Если запись добавлена
                if ($id) {
                    // Проверим, загружалось ли через форму изображение
                    if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                        // Если загружалось, переместим его в нужную папке, дадим новое имя
                        move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/data/applications/{$id}.jpg");
                    }
                };

                $client_ticket = $id;
        		$client_serial = $serial;
            }
		}

		require_once(ROOT . '/views/applications/calculation.php');
		return true;
	}

}
