<?php

class AdminPricesController extends AdminBase
{
	public function actionCreateCategory()
	{
		// Проверка доступа
        self::checkAdmin();

        $name = '';
        $sort_order = '';

        $errors = false;

		if(isset($_POST['submit'])) {

			$name = $_POST['name'];
			$sort_order = $_POST['sort_order'];
			$status = $_POST['status'];

			// Валидация полей
	        if (!Admin::checkFill($name, 5)) {
	            $errors[] = 'Название не должно быть короче 5 символов.';
	        }
	        if(filter_var($sort_order, FILTER_VALIDATE_INT) === false)
				$errors[] = 'Порядок сортировки должен иметь целочисленое значение.';

	        if ($errors == false) {
                // Если ошибок нет

                Prices::addCategory($name, $sort_order, $status);

                header("Location: /admin/prices");
            }


		}


        require_once(ROOT . '/views/admin_prices/createCategory.php');
        return true;
	}


	public function actionUpdateCategory($id)
	{
		// Проверка доступа
        self::checkAdmin();

        $categoryItem = Prices::getCategoryItemById($id);

        $name = $categoryItem['name'];
        $sort_order = $categoryItem['sort_order'];
        $status = $categoryItem['status'];

        $errors = false;

		if(isset($_POST['submit'])) {

			$name = $_POST['name'];
			$sort_order = $_POST['sort_order'];
			$status = $_POST['status'];

			// Валидация полей
	        if (!Admin::checkFill($name, 5)) {
	            $errors[] = 'Название не должно быть короче 5 символов.';
	        }
	        if(filter_var($sort_order, FILTER_VALIDATE_INT) === false)
				$errors[] = 'Порядок сортировки должен иметь целочисленое значение.';

	        if ($errors == false) {
                // Если ошибок нет

                Prices::updateCategory($id, $name, $sort_order, $status);

                header("Location: /admin/prices");
            }


		}


        require_once(ROOT . '/views/admin_prices/updateCategory.php');
        return true;
	}

	public function actionDeleteCategory($id)
    {
        // Проверка доступа
        self::checkAdmin();

        Prices::deleteCategory($id);

        header("Location: /admin/prices");

        return true;
    }

	public function actionUpdateAddition($id)
	{
		// Проверка доступа
        self::checkAdmin();

        $additionItem = Prices::getAdditionItemById($id);

        $name = $additionItem['name'];
        $price = $additionItem['price'];

        $errors = false;

		if(isset($_POST['submit'])) {

			$price = $_POST['price'];

			// Валидация полей
	        if(filter_var($price, FILTER_VALIDATE_INT) === false)
				$errors[] = 'Цена должна иметь целочисленое значение.';

	        if ($errors == false) {
                // Если ошибок нет

                Prices::updateAddition($id, $price);

                header("Location: /admin/prices");
            }


		}


        require_once(ROOT . '/views/admin_prices/updateAddition.php');
        return true;
	}

	public function actionCreatePrice()
	{
		// Проверка доступа
        self::checkAdmin();

        $categoryList = array();
		$categoryList = Admin::getPricesList();

        $name = '';
        $price = '';
        $category = 0;

        $errors = false;

		if(isset($_POST['submit'])) {

			$name = $_POST['title'];
			$price = $_POST['price'];
			$category = $_POST['category'];

			// Валидация полей
	        if (!Admin::checkFill($name, 5)) {
	            $errors[] = 'Название не должно быть короче 5 символов.';
	        }
	        if(filter_var($price, FILTER_VALIDATE_INT) === false)
				$errors[] = 'Цена должна иметь целочисленое значение.';

	        if ($errors == false) {
                // Если ошибок нет

                Prices::addPrice($name, $price, $category);

                header("Location: /admin/prices");
            }


		}


        require_once(ROOT . '/views/admin_prices/createPrice.php');
        return true;
	}


	public function actionUpdatePrice($id)
	{
		// Проверка доступа
        self::checkAdmin();

        $priceItem = Prices::getPriceItemById($id);

        $categoryList = array();
		$categoryList = Admin::getPricesList();

        $name = $priceItem['title'];
        $price = $priceItem['price'];
        $category = $priceItem['prices_name_id'];

        $errors = false;

		if(isset($_POST['submit'])) {

			$name = $_POST['title'];
			$price = $_POST['price'];
			$category = $_POST['category'];

			// Валидация полей
	        if (!Admin::checkFill($name, 5)) {
	            $errors[] = 'Название не должно быть короче 5 символов.';
	        }
	        if(filter_var($price, FILTER_VALIDATE_INT) === false)
				$errors[] = 'Цена должна иметь целочисленое значение.';

	        if ($errors == false) {
                // Если ошибок нет

                Prices::updatePrice($id, $name, $price, $category);

                header("Location: /admin/prices");
            }


		}


        require_once(ROOT . '/views/admin_prices/updatePrice.php');
        return true;
	}

	public function actionDeletePrice($id)
    {
        // Проверка доступа
        self::checkAdmin();

        Prices::deletePrice($id);

        header("Location: /admin/prices");

        return true;
    }
    
}